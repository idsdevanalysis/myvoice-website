import React, { Component } from 'react'
import Link from 'next/link'

class Footer extends Component {
  render () {
    return (
      <>
        <footer className="space--sm footer-2 bg--dark">
          <div className="container">
            <div className="row justify-content-md-center">
              <div className="col-sm-6 col-md-3 col-xs-6">
                <h6 className="type--uppercase">A Propos</h6>
                <ul className="list--hover">
                  <li><Link href="/a-propos"><a>Qui sommes-nous</a></Link></li>
                  <li><Link href="/a-propos"><a>Rejoindre notre équipe</a></Link></li>
                  <li><Link href="/a-propos"><a>Nous contacter</a></Link></li>
                </ul>
              </div>
              <div className="col-sm-6 col-md-3 col-xs-6">
                <h6 className="type--uppercase">MyVoice 974</h6>
                <ul className="list--hover">
                  <li><Link href="/re-confidentialite">Politique de confidentialité</Link></li>
                  <li><Link href="/re-condition-general-utilisation">Condition générale d&apos;utilisation</Link></li>
                  <li><a href="/re-mention-legale">Mention Légale</a></li>
                  <li><Link href="/faq"><a>FAQ</a></Link></li>
                  <li><a href="https://www.facebook.com/myvoice.re" target="_blank" rel="noreferrer">Facebook</a></li>
                </ul>
              </div>
            </div>
          </div>
        </footer>
        <div style={{ textAlign: 'center' }}>Tous droits réservés - IDS 2021</div>
      </>
    )
  }
}

export default Footer
