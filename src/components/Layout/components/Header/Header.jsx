import React, { Component } from 'react'
import Link from 'next/link'

class Header extends Component {
  render () {
    return (
      <div className="nav-container">
        <div className="via-1610518010687" via="via-1610518010687" vio="Main Menu">
          <div className="bar bar--sm visible-xs">
            <div className="container">
              <div className="row">
                <div className="col-3 col-md-2">
                  <Link href="/"><a><img className="logo logo-dark" alt="logo" src="/img/MYVOICE_974.png" /> <img className="logo logo-light" alt="logo" src="/img/MYVOICE_974.png" /> </a></Link>
                </div>
                <div className="col-9 col-md-10 text-right">
                  <a href="/#cest-quoi-myvoice-" className="hamburger-toggle" data-toggle-class="#menu1;hidden-xs hidden-sm"> <i className="icon icon--sm stack-interface stack-menu"></i> </a>
                </div>
              </div>
            </div>
          </div>
          <nav id="menu1" className="bar bar-1 hidden-xs">
            <div className="container">
              <div className="row">
                <div className="col-lg-1 col-md-2 hidden-xs">
                  <div className="bar__module">
                    <Link href="/"><a> <img className="logo logo-dark" alt="logo" src="/img/MYVOICE_974.png" /> <img className="logo logo-light" alt="logo" src="/img/MYVOICE_974.png" /> </a></Link>
                  </div>
                </div>
                <div className="col-lg-3 text-left text-left-xs text-left-sm col-md-1 app-menu-hor left-nav">
                  <div className="bar__module">
                    <ul className="menu-horizontal text-left">
                      <li>
                        <a href="/#cest-quoi-myvoice-" className="inner-link" target="_self">C&apos;est quoi myvoice ?</a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="col-lg-8 text-right text-left-xs text-left-sm col-md-7 app-menu-hor">
                  <div className="bar__module">
                    <a className="btn btn--sm btn--primary type--uppercase" href="/sinscrire"> <span className="btn__text">M&apos;inscrire</span></a>
                    &nbsp;
                    <a className="btn btn--sm type--uppercase" href="/contact"> <span className="btn__text">nous contacter</span> </a>
                    &nbsp;
                    <a className="btn btn--sm type--uppercase" href="https://research.kantartns.io/community/reunion/myvoice/Account/LogOn?ReturnUrl=%2Fcommunity%2Freunion%2Fmyvoice%2F"> <span className="btn__text">Me Connecter</span></a>
                  </div>
                </div>
              </div>
            </div>
          </nav>
        </div>
      </div>
    )
  }
}

export default Header
