import React, { Component } from 'react'
import Header from './components/Header/Header'
import Footer from './components/Footer/Footer'
import PropTypes from 'prop-types'

class Layout extends Component {
  render () {
    return (
      <>
        <Header />
        <div className="main-container">
          {this.props.children}
          <Footer />
        </div>
      </>
    )
  }
}
Layout.propTypes = {
  children: PropTypes.any
}

export default Layout
