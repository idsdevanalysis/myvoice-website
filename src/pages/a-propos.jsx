import React, { Component } from 'react'
import Head from 'next/head'

class AboutPage extends Component {
  render () {
    return (
      <>
        <Head>
          <title>Qui sommes-nous - myvoice</title>
          <meta name="description" content="Notre communauté : des personnes de tout âge qui participent dans des discussions sur plusieurs sujets d’actualité ou sur des marques." />
        </Head>
        <a id="a-propos" className="in-page-link"></a>
        <section className="space--xxs">
          <div className="container">
            <div className="row">
              <div className="col-md-12">
                <h1>A propos</h1>
              </div>
            </div>
          </div>
        </section>
        <a id="accordion-a-propos" className="in-page-link"></a>
        <section className="switchable space--xxs">
          <div className="container">
            <div className="row justify-content-between">
              <div className="switchable__text col-md-7">
                <ul className="accordion accordion-1">
                  <li>
                    <div className="accordion__title"> <span className="h5"><p>Qui sommes-nous</p></span> </div>
                    <div className="accordion__content">
                      <p className="lead"> MyVoice organise et gère des panels de consommateurs (le(s) « panel(s) ») qui sont des groupes représentatifs de personnes participant à une ou plusieurs études.&nbsp;<br />MyVoice dispose d’un des panels de consommateurs dans les îles de l’océan Indien, ce qui lui permet de proposer à ses clients des études et des analyses fiables sur les habitudes de vie et de consommation et le comportement des consommateurs dans la zone océan Indien.</p>
                      <p className="lead">Les Panélistes peuvent adhérer et participer à un ou plusieurs panels de consommateurs et/ou une ou plusieurs études, soit Analysis Co Ltd<strong>, enregistrée au Registre du Commerce, sous le numéro C08014948, dont le siège social est situé à Analysis France SARL enregistrée au Registre du Commerce, sous le numéro 880 865 274, 8 rue de l’Amitié, ZAC du Triangle 97490, Sainte Clotilde, Réunion.</strong></p>
                    </div>
                  </li>
                  <li>
                    <div className="accordion__title"> <span className="h5"><p>Rejoindre notre équipe</p></span> </div>
                    <div className="accordion__content">
                      <p className="lead"> Envie de faire émerger des insights passionnants, dans une entreprise mondiale qui ouvre la voie à la connaissance et à la compréhension de l&apos;humain ? <br /></p>
                      <p className="lead">Rejoignez notre équipe et contribuez à stimuler la croissance. </p>
                    </div>
                  </li>
                  <li>
                    <div className="accordion__title"> <span className="h5"><p>Nous contacter</p></span> </div>
                    <div className="accordion__content">
                      <p className="lead">Email : myvoice974@ids.ac</p>
                    </div>
                  </li>
                </ul>
              </div>
              <div className="col-lg-5 col-md-5"> <img alt="Image" className="border--round box-shadow-wide" src="/img/inner-2.jpg" /> </div>
            </div>
          </div>
        </section>
      </>
    )
  }
}

export default AboutPage
