import React, { Component } from 'react'
import Head from 'next/head'

class MuTermsCondition extends Component {
  render () {
    return (
      <>
        <Head>
          <title>Conditions Générales de Services - Réunion - myvoice</title>
        </Head>
        <section className="cover imagebg text-center height-50" data-overlay="3">
          <div className="background-image-holder"> <img alt="background" src="img/landing-3.jpg" /> </div>
          <div className="container pos-vertical-center">
            <div className="row">
              <div className="col-md-12">
                <h1>Conditions Générales de Services</h1>
              </div>
            </div>
          </div>
        </section>

        <section>
          <div className="container">
            <div className="row justify-content-center">
              <div className="col-md-12 col-lg-12">
                <article>
                  <div className="et_pb_module et_pb_text et_pb_text_0  et_pb_text_align_left et_pb_bg_layout_light">
                    <div className="et_pb_text_inner">
                      <p>Version 1.0 1.0</p>
                      <p>Date de la dernière mise à jour : 09 juillet 2020</p>
                      <p>Plan :</p>
                      <ul>
                        <li><a href="#partie1">1. Objet</a></li>
                        <li><a href="#partie2">2. Description des services de</a><span style={{ color: '#20a6db' }}> MyVoice</span></li>
                        <li><a href="#partie3">3. Inscription</a></li>
                        <li><a href="#partie4">4. Points cadeaux</a></li>
                        <li><a href="#partie5">5. Obligations du panéliste</a></li>
                        <li><a href="#partie6">6. Obligations de MyVoice</a></li>
                        <li><a href="#partie7">7. Contenus des panélistes</a></li>
                        <li><a href="#partie8">8. Propriété intellectuelle</a></li>
                        <li><a href="#partie9">9. Données personnelles</a></li>
                        <li><a href="#partie10">10. Liste d’opposition au démarchage téléphonique pour les panélistes</a></li>
                        <li><a href="#partie11">11. Liens hypertextes</a></li>
                        <li><a href="#partie12">12. Durée et résiliation</a></li>
                        <li><a href="#partie13">13. Modification du contrat</a></li>
                        <li><a href="#partie14">14. Notifications</a></li>
                        <li><a href="#partie15">15. Stipulations diverses</a></li>
                        <li><a href="#partie16">16. Droit applicable, compétence juridictionnelle et médiation</a></li>
                      </ul>
                      <p>&nbsp;</p>
                      <h3 id="partie1">1. Objet</h3>
                      <p>Les présentes conditions générales de services (les « CGS ») définissent les conditions contractuelles selon lesquelles les panélistes peuvent adhérer et participer à un ou plusieurs panels de consommateurs et/ou une ou plusieurs études de Analysis France SARL, soit Analysis Co Ltd<strong>, enregistrée au Registre du Commerce, sous le numéro C08014948, dont le siège social est situé à 8 rue de l’Amitié, ZAC du Triangle 97490, Sainte Clotilde, Réunion, Rue du Judiciaire, 72201 – Ebene, Ile Maurice (</strong>Analysis Co Ltd).</p>
                      <p>La simple consultation des pages web des sites Internet et/ou applications mobiles dédiés aux panélistes de MyVoice, édités par MyVoice ou ses sous-traitants (les « sites ») ne nécessite pas de conclure un contrat avec MyVoice. En revanche, l’adhésion et la participation aux panels de consommateurs et/ou à une ou plusieurs études de MyVoice sont soumises à la conclusion d’un contrat avec MyVoice (le « contrat »).</p>
                      <p>Les présentes CGS sont complétées par des conditions particulières de services applicables à certains services, certains panels et/ou certaines études.</p>
                      <p>Le refus de conclure le Contrat implique l’impossibilité de participer aux panels de consommateurs et/ou aux études de MyVoice.</p>
                      <p>Les candidats souhaitant devenir panélistes sont invités à lire attentivement les CGS avant de les accepter pour participer à un panel de consommateurs et/ou à des études de qualité de MyVoice.</p>
                      <h3 id="partie2">2. Description des services de MyVoice</h3>
                      <p>MyVoice, la division de Analysis France SARL dédiée aux panels de consommateurs, dispose d’un des panels de consommateurs dans les îles de l’océan Indien, ce qui lui permet de proposer à ses clients des études et des analyses fiables sur les habitudes de vie et de consommation et le comportement des consommateurs dans la zone océan Indien.</p>
                      <p>Un panel de consommateurs est une représentation en modèle réduit des foyers ou individus vivant en dans une des îles de l’océan Indien, sélectionnés selon des critères précis.</p>
                      <p>MyVoice collecte un large éventail d’informations concernant les participants à ses panels et/ou études (les « panélistes ») relatives au profil sociodémographique, à l’exposition média (télévision, radio, presse, internet), à la consommation ou encore au mode de vie. Ces informations permettent à MyVoice de réaliser des études et analyses qui peuvent être extrapolées à l’ensemble de la population, de suivre les habitudes de vie et de consommation, leurs comportements et leurs avis de consommateurs. MyVoice peut ensuite délivrer à ses clients des études et analyses, agrégées (anonymes) ou désagrégées (non anonymes), sur les habitudes de vie et consommation et le comportement des consommateurs qui seraient clients ou prospects des clients de MyVoice afin de leur permettre d’affiner la connaissance de leurs clients et prospects et d’améliorer la personnalisation de leurs produits, services ou actions marketing. Pour plus d’informations sur la manière dont les données sont collectées et traitées par MyVoice, le candidat panéliste et le panéliste sont invités à se reporter à la&nbsp;politique de confidentialité de MyVoice.</p>
                      <p>Pour récompenser ses panélistes de leur adhésion et de leur participation à ses panels de consommateurs et/ou à ses études, MyVoice leur attribue des points (les « points »), cumulés sur un compte (« Espace cadeau »), qu’ils peuvent échanger contre des cadeaux sur un Site dédié exclusivement aux panélistes, des chèques cadeaux ou à des bons d’achat, dont la gestion est effectuée par MyVoice .</p>
                      <h3 id="partie3">3. Inscription</h3>
                      <p><strong>3.1. Prérequis technique.</strong>&nbsp;Pour proposer sa participation aux panels de consommateurs et/ou à une étude MyVoice et pour pouvoir participer, après sa sélection par MyVoice, aux panels de consommateurs et/ou aux études de MyVoice, le candidat panéliste doit être équipé :</p>
                      <ul>
                        <li>d’un ordinateur et, pour la participation à certains panels ou études, d’un smartphone (un iPhone, un mobile Android ou un Windows Phone par exemple) ou d’une tablette tactile (un iPad, une tablette Android ou une tablette Windows par exemple) ;</li>
                        <li>d’une connexion à Internet.</li>
                      </ul>
                      <p><strong>3.2. Conditions d’inscription.</strong>&nbsp;La participation aux panels de consommateurs et/ou aux études de MyVoice est fondée sur le volontariat.</p>
                      <p>La possibilité de proposer sa participation aux panels de consommateurs et/ou aux études de MyVoice sur les sites de MyVoice est réservée aux personnes domiciliées dans la zone océan Indien (ile Maurice, ile de La Réunion, Madagascar, Mayotte, Seychelles, Comores et aux Maldives) :</p>
                      <ul>
                        <li>capables juridiquement de souscrire un contrat, c’est-à-dire majeures non frappées par une incapacité ou mineures émancipées ; ou,</li>
                        <li>mineures âgées de 15 ans et plus, sous réserve de l’accord de la ou les personne(s) exerçant l’autorité parentale. MyVoice se réserve, à cet égard, le droit de réclamer un justificatif (attestation sur l’honneur ou tout autre document rendu obligatoire par la loi). Les parents ou toute autre personne exerçant l’autorité parentale sont tenus de surveiller la participation de leur(s) mineur(s) aux panels de consommateurs et/ou aux études de MyVoice.</li>
                      </ul>
                      <p><strong>3.3. Procédure d’inscription.</strong>&nbsp;Pour proposer sa participation aux panels de consommateurs et/ou à une ou plusieurs études de MyVoice, le candidat panéliste doit suivre la procédure d’inscription qui requiert de :</p>
                      <ul>
                        <li>fournir une adresse email qui servira à identifier et contacter le candidat panéliste ;</li>
                        <li>fournir des informations relatives à son identité, ses coordonnées et son foyer nécessaires pour permettre à MyVoice de sélectionner les candidats panélistes en fonction des besoins de chaque panel et/ou de chaque étude.</li>
                      </ul>
                      <p>Le candidat panéliste garantit le caractère exact, complet et à jour des informations communiquées. Dans l’hypothèse où le candidat panéliste renseigne, lors de son inscription, des informations fausses, périmées, incomplètes, trompeuses ou de nature à induire en erreur et susceptibles de remettre en cause la décision de MyVoice d’accepter la participation du candidat panéliste, MyVoice pourra mettre fin sans délai au contrat et donc à sa participation aux panels de consommateurs et/ou aux études (dans les conditions décrites à l’article 12).</p>
                      <ul>
                        <li>certifier être une personne majeure ou une personne mineure de 15 ans et plus disposant de l’accord de la ou des personne(s) exerçant l’autorité parentale ;</li>
                        <li>prendre connaissance et accepter les présentes CGS, les CPS applicables et la&nbsp;politique de confidentialité de MyVoice relative au traitement des données à caractère personnel des panélistes, candidats panélistes et visiteurs des sites. En cochant en ligne la case attestant de la lecture et l’acceptation par le candidat panéliste des CGS et des CPS applicables (et notamment des CPS de la Boutique cadeaux), le candidat panéliste reconnaît qu’il est pleinement informé et qu’il est tenu par l’ensemble des stipulations des CGS et des CPS.</li>
                      </ul>
                      <p>Une fois la procédure d’inscription finalisée, le candidat panéliste reçoit une confirmation de la prise en compte de sa demande de participation par email.</p>
                      <p><strong>Le contrat entre le candidat panéliste et MyVoice n’est alors pas encore définitivement conclu.</strong></p>
                      <p><strong>3.4. Acceptation ou refus de la proposition de participation aux panels et/ou aux études.</strong>&nbsp;MyVoice analyse chaque demande de participation à ses panels de consommateurs et/ou à une étude dans les meilleurs délais au regard des besoins des panels et/ou études actuellement en cours ou à venir, et des informations fournies par le candidat panéliste.</p>
                      <p>Un tel processus de sélection est nécessaire car les panels et études doivent porter sur des échantillons suffisamment représentatifs de la population d’un des pays de la région océan Indien (exemple : mauricienne ou réunionnaise)&nbsp; ou de certains segments de la population mauricienne (par exemple, les personnes âgées de plus de 50 ans).</p>
                      <p>MyVoice pourra être amenée à décliner la proposition de participation d’un candidat souhaitant devenir panéliste pour de multiples raisons telles que les critères sociodémographiques nécessaires à assurer la représentativité de la population d’une des îles sélectionnées (mauricienne, réunionnaise, etc. ) (âge, sexe, région, nombre de personnes composant le foyer, etc.) et les critères relatifs à l’équipement informatique (port USB, PC, accès à Internet au domicile, type de système d’exploitation, etc.).</p>
                      <p>L’absence de notification par MyVoice de l’acceptation de la proposition de participation d’un candidat panéliste aux panels et/ou à une étude dans un délai d’un (1) an à compter de sa demande de participation doit être considérée comme un refus. MyVoice s’engage, à ce titre, à supprimer les données à caractère personnel collectées lors de sa demande de participation dans un délai d’un mois</p>
                      <p>En cas de refus de sa proposition de participation aux panels et/ou à une étude, le candidat panéliste accepte le fait que MyVoice ne sera pas tenue de justifier sa décision. Aucun contrat ne sera alors conclu entre MyVoice et le candidat panéliste.</p>
                      <p><strong>En cas d’acceptation de la proposition de participation du candidat panéliste, le contrat entre ce dernier et MyVoice sera définitivement conclu.</strong></p>
                      <p>Le candidat panéliste, qui deviendra alors un panéliste, recevra un email lui confirmant sa participation et précisant :</p>
                      <ul>
                        <li>son nom et prénom et, s’il est mineur, le nom, prénom, adresse et mail de la ou les personnes exerçant l’autorité parentale ;</li>
                        <li>la date de conclusion du contrat (à savoir la date d’émission de l’email informant le candidat panéliste de l’acceptation de sa proposition de participation) ;</li>
                        <li>le ou les panels et/ou études auxquels il participera ;</li>
                        <li>les présentes CGS ;</li>
                        <li>les CPS applicables ;</li>
                        <li>la politique de confidentialité de MyVoice ;</li>
                        <li>les points gagnés lors de la validation de sa participation.</li>
                      </ul>
                      <p>Le panéliste s’engage à vérifier le contenu de cet email et communiquera par écrit toute éventuelle erreur à MyVoice dans les plus brefs délais. Il conservera la confirmation de sa participation. Elle pourra lui servir de preuve.</p>
                      <p>Pour compléter son adhésion en tant que panéliste, le panéliste, ainsi éventuellement que les membres de son foyer, devront préciser d’autres informations à MyVoice et renseigner notamment :</p>
                      <ul>
                        <li>des informations relatives à la composition du foyer : adresse postale, composition du foyer, naissance attendue, catégorie de revenus ;</li>
                        <li>des informations relatives à la personne du panéliste et éventuellement aux autres membres de son foyer : civilité, nom, prénom, lien de parenté, sexe, date de naissance, photo de la carte d&apos;identité obligatoire;</li>
                        <li>des informations sociodémographiques sur le panéliste et éventuellement les autres membres de son foyer : situation professionnelle, niveau d’études, catégorie socioprofessionnelle, taille, poids, mensurations, revenus ;</li>
                        <li>des informations relatives aux animaux, biens et équipements du panéliste et éventuellement des autres membres de son foyer : chat, chien, téléphone portable, tablette, résidence, voiture, équipement électroménager, équipement hifi multimédia, jardin/potager/verger ;</li>
                        <li>des informations relatives au mode de vie et au comportement d’achat et de consommation du panéliste et éventuellement des autres membres de son foyer : fréquences d’achats de certaines catégories de produits (textile, hygiène, coiffure), lieux d’achats.</li>
                      </ul>
                      <p>Le panéliste certifie que les données communiquées seront exactes, à jour et complètes. Il est tenu de signaler à MyVoice dans les meilleurs délais tout changement relatif aux données communiquées, notamment dans le cadre du recensement envoyé annuellement par MyVoice.</p>
                      <p>Tout renseignement d’informations fausses, périmées, incomplètes, trompeuses ou de nature à induire en erreur constitue une violation du contrat susceptible dans certains cas d’entraîner la résiliation du contrat dans les conditions définies à l’article 12. En cas de fraude avérée, MyVoice se réserve en outre le droit de demander au panéliste fraudeur la réparation des préjudices causés à MyVoice.</p>
                      <p><strong>3.5. Mot de passe.</strong>&nbsp;Lors de l’inscription, le candidat panéliste est invité à fournir une adresse email et créer un mot de passe. MyVoice adresse au candidat panéliste un lien de confirmation par afin d’accéder et participer aux panels de consommateurs et/ou études de MyVoice.</p>
                      <p>Pour se prémunir contre les personnes qui tentent de deviner les mots de passe, MyVoice limite le nombre d’essais d’accès aux espaces personnels des panélistes dans un temps donné.</p>
                      <p>Cette limitation ne supprimant pas le risque d’une connexion frauduleuse d’un tiers, MyVoice recommande aux panélistes de suivre les recommandations préconisées par le Data Protection Act.</p>
                      <p>La connexion du panéliste à l’aide de ses identifiants de connexion (notamment le mot de passe) donne à MyVoice la garantie de base de l’identité du panéliste qui procède à la connexion. La sécurité de ces identifiants est donc essentielle.</p>
                      <p>Le panéliste devra s’assurer du respect de la confidentialité de ces identifiants et devra veiller à se déconnecter effectivement de son espace personnel après avoir visité les sites, en particulier lorsqu’il y accède à partir d’un ordinateur public.</p>
                      <p>Les mots de passe ne doivent en aucun cas être communiqués ni partagés avec des tiers. Les mots de passes sont enregistrés par MyVoice sous forme chiffrée et ne sont pas accessibles au personnel de MyVoice. Un représentant de MyVoice ne demandera jamais le mot de passe d’un panéliste.</p>
                      <p>Dans le cas où le panéliste suspecterait une perte ou une divulgation involontaire de son mot de passe ou d’éléments susceptibles de reconstituer ce mot de passe, le panéliste devra immédiatement en informer MyVoice selon les modalités suivantes, afin qu’il soit procédé à un changement de mot de passe.</p>
                      <p>Il est recommandé de privilégier la prise de contact par téléphone ou via Internet qui est plus rapide :</p>
                      <p><strong>Par téléphone en appelant le numéro suivant</strong>&nbsp;:&nbsp;</p>
                      <p>+230 202 0055</p>
                      <p><strong>Par email à l’adresse suivante</strong>&nbsp;:</p>
                      <p><a href="mailto:myvoice974@ids.ac">myvoice974@ids.ac</a></p>
                      <p><strong>Par le biais de l’espace personnel du panéliste</strong>&nbsp;:</p>
                      <p>En cliquant sur « Mot de passe oublié ? » ou « Mot de passe perdu ? »</p>
                      <p>MyVoice s’engage le cas échéant à bloquer tout accès non autorisé à l’espace personnel du panéliste dans les meilleurs délais à compter de la notification par le panéliste.</p>
                      <p>En l’absence de notification ou durant la période précédant le blocage des accès non autorisés par MyVoice, MyVoice ne peut être tenue responsable des conséquences potentiellement préjudiciables d’un accès par un tiers non autorisé à l’espace personnel du panéliste via lesdits identifiants de connexion sauf si la divulgation à ce tiers des identifiants du panéliste est causée par sa propre faute ou négligence.</p>
                      <p>En conséquence, hors cas de divulgation d’identifiants causée par une faute ou une négligence de MyVoice, le panéliste accepte que toutes les actions liées à son espace personnel seront considérées comme étant effectuées par lui-même en l’absence de notification d’une perte, d’un vol ou d’une utilisation non autorisée de ses identifiants de connexion, et notamment :</p>
                      <ul>
                        <li>toute connexion à son espace personnel ;</li>
                        <li>toute participation à un panel ou à une étude ;</li>
                        <li>toute conversion de points sur l’Espace cadeaux.</li>
                      </ul>
                      <p>Cette présomption pouvant être renversée, le panéliste pourra néanmoins en rapporter la preuve contraire et démontrer l’existence d’une fraude imputable à un tiers.</p>
                      <h3 id="partie4">4. points cadeaux</h3>
                      <p><strong>4.1. Définitions.</strong>&nbsp;La conclusion du contrat, intégrant les&nbsp;CPS de l’Espace cadeaux, permet au panéliste d’accéder à son espace personnel sur les sites, de consulter son solde de points et de commander, via l’Espace cadeaux, des cadeaux (les « cadeaux »), selon les conditions définies dans lesdites CPS.</p>
                      <p>La valeur des points par rapport aux cadeaux est définie par MyVoice.</p>
                      <p>Les points attribués à un panéliste sont transférés sur le compte-points que MyVoice conserve pour chaque panéliste. Le compte-points ne constitue pas un compte bancaire ou financier réel. Les points conservés ne génèrent pas d’intérêts.</p>
                      <p>Les cadeaux, accessibles sur la Boutique cadeaux, désignent les produits et prestations de service multiples et variés (petit électroménager, articles high-tech, bricolage, jardinage, loisirs-sorties, chèques cadeaux, don à une association, etc.), fournis aux panélistes, en échange de la conversion des points cumulés sur leurs Comptes-points.</p>
                      <p><strong>4.2. Attribution de points au panéliste.</strong></p>
                      <p>Le panéliste reçoit notamment des points lors :<br/>
                      <ul>
                        <li>&bull; de l&apos;acceptation par MyVoice de sa participation ;</li>
                        <li>&bull; de sa participation à des études spécifiques et continues, par exemple lors de l&apos;enregistrement de ses achats dans le cadre d&apos;un panel (études de type panel) ;</li>
                        <li>&bull; de sa participation à des études spécifiques et ponctuelles (études&nbsp;<em>ad hoc</em>).</li>
                      </ul>
                      </p>
                      <p>Le nombre de points à gagner lors de chacun de ces événements est fixé par MyVoice. Il peut notamment varier en fonction de la complexité et la durée des questionnaires que le panéliste sera amené à renseigner.</p>
                      <p>Le nombre de points à gagner est communiqué au panéliste :
                      <ul>
                        <li>&bull; au cours de sa procédure d&apos;inscription pour les points associés à l&apos;acceptation de sa participation ;</li>
                        <li>&bull; dans la proposition du questionnaire qui lui est envoyée, pour les études&nbsp;<em>ad hoc</em>;</li>
                        <li>&bull; lors de sa participation hebdomadaire, pour les études de type panel, notamment à l&apos;occasion de la déclaration des paniers de la semaine.</li>
                      </ul>
                      </p>
                      <p>Pour les points associés au renseignement d&apos;un questionnaire, les points ne sont acquis que lorsque toutes les réponses ont été renseignées et sont authentiques.</p>
                      <p>MyVoice suit et mesure la qualité de participation des panélistes et peut, à ce titre, être amenée à contacter un panéliste (par email, téléphone ou courrier postal), dans le cas notamment d&apos;un changement des habitudes de consommation. Il convient, à cet égard, de préciser que tout questionnaire concernant la qualité ne donne pas droit à des points.</p>
                      <p>L&apos;intélligence artificielle permet d&apos;identifier si un panéliste répond de manière systématique. Les resultats de cette analyse pourront être utilisés pour annuler toute récompense dû au dit paneliste.</p>
                      <p>Il convient aussi de préciser que certains questionnaires de recueil de consentement ne donnent pas lieu à l&apos;attribution de points.</p>
                      <p>Les points se cumulent durant toute la période de participation du panéliste et ne sont jamais retirés au panéliste sauf fraude du panéliste.</p>
                      <p>Tout Point acquis par un panéliste, ayant fourni pour ce faire des informations incorrectes, fausses, périmées, incomplètes, trompeuses, lui sera retiré par MyVoice, sans préjudice d&apos;autres actions, et notamment de la résiliation du contrat dans les conditions définies à l&apos;article 12.</p>
                      <p>Les panélistes peuvent consulter, à tout moment, le solde de points cumulés en accédant à leur espace personnel sur les sites et, notamment via l&apos;Espace cadeaux à leur compte-points.</p>
                      <p><strong>4.3. Conversion des points.</strong>&nbsp;Les panélistes peuvent utiliser les points cumulés sur leur compte-points pour les échanger contre les cadeaux figurant sur le catalogue de l’Espace cadeaux, qui peuvent consister en :</p>
                      <ul>
                        <li>des produits ou prestations de services ;</li>
                        <li>des chèques-cadeaux ;</li>
                        <li>un don à un organisme caritatif sélectionné par MyVoice.</li>
                      </ul>
                      <p>Myvoice se reserve le droit de ne pas distribuer le cadeau commandé a tout panéliste qui aurait fourni de fausses informations dans son questoinnaire de profil.</p>
                      <p><strong>Les points ne donnent jamais droit à une contrepartie monétaire (Roupies mauriciennes, euro, etc.).</strong></p>
                      <p>MyVoice se réserve le droit, à tout moment, de modifier le catalogue des cadeaux accessible sur l’Espace cadeaux ainsi que le nombre de points cumulés nécessaires en vue de leur obtention. Pour plus d’informations,&nbsp;MyVoice invite les panélistes à lire les CPS de l’Espace cadeaux.</p>
                      <h3 id="partie5">5. Obligations du panéliste</h3>
                      <p><strong>5.1. Participation aux panels et aux études.</strong>&nbsp;Pour participer aux panels et/ou études de MyVoice, le panéliste fournira notamment des informations détaillées sur sa consommation, en enregistrant ses achats et/ou en participant à des études sous forme de questionnaires, afin de permettre à MyVoice de faire des études de marché et notamment de connaître l’évolution de la consommation, les tendances, et permettre à ses clients de créer de nouveaux produits ou services ou modifier ceux existants ainsi qu’adapter leur communication, promotion et publicités.</p>
                      <p>Dans le cadre de sa participation aux panels et/ou études, le panéliste s’engage à communiquer régulièrement à MyVoice ses achats selon les instructions fournies par la suite par MyVoice.</p>
                      <p>Le panéliste accepte que&nbsp;MyVoice le contacte par email, téléphone ou courrier postal et/ou le sollicite pour répondre à des questionnaires additionnels et participer aux études de MyVoice.</p>
                      <p><strong>5.2. Utilisation des outils de MyVoice.</strong>&nbsp;MyVoice pourra remettre au panéliste des outils de recueil d’informations, et notamment un outil de scan des achats.</p>
                      <p>Sauf indication contraire, MyVoice reste propriétaire des outils remis aux panélistes.</p>
                      <p>Le panéliste s’engage à utiliser les outils qui peuvent lui être remis par MyVoice, comme les outils de scan d’achat conformément aux instructions qui lui seront communiquées.</p>
                      <p>Il s’engage à manipuler avec précaution ces outils et à les entretenir. Il s’engage à signaler à MyVoice tout dysfonctionnement d’un tel outil.</p>
                      <p>En cas de résiliation du contrat, le panéliste s’engage à restituer tous les outils qui lui auront été remis par MyVoice dans les plus brefs délais. Dans l’hypothèse où les outils prêtés au panéliste ne seraient pas restitués, MyVoice se réserve le droit de demander une compensation financière.</p>
                      <p><strong>5.3. Interdictions.</strong>&nbsp;Le panéliste s’interdit :</p>
                      <ul>
                        <li>de violer le contrat ;</li>
                        <li>de s‘inscrire au nom d’une autre personne ;</li>
                        <li>de créer une fausse identité de nature à induire&nbsp;MyVoice en erreur (faux profil) ;</li>
                        <li>de refuser de fournir la confirmation de son identité ou de toute information fournie ;</li>
                        <li>d’utiliser toute ou partie des panels de consommateurs et/ou études à des fins professionnelles ;</li>
                        <li>de reproduire, copier, vendre, revendre, échanger ou exploiter dans un but commercial ou non commercial, tout ou partie des sites de MyVoice et de leur contenu ;</li>
                        <li>de diffuser des informations délibérément fausses, inexactes ou trompeuses ;</li>
                        <li>d’envoyer à MyVoice ou d’injecter sur les sites des éléments contenant des virus, chevaux de Troie, programmes informatiques pouvant endommager, gêner, intercepter subrepticement ou exproprier tous systèmes d’information ;</li>
                        <li>d’utiliser tout robot, spider, autre dispositif automatique ou procédure manuelle permettant de contrôler ou de copier les sites sans l’autorisation écrite préalable de MyVoice ;</li>
                        <li>de se faire passer pour le représentant ou un employé de MyVoice ;</li>
                        <li>de détourner les finalités des outils de scan d’achats, des sites de MyVoice et tout support de communication et de contact ;</li>
                        <li>de porter atteinte à l’image de marque de MyVoice, sur tout support de communication (médias, réseaux sociaux, web), par quelque moyen que ce soit.</li>
                      </ul>
                      <p>Le panéliste reconnaît et accepte que les commentaires qu’il poste, sur tout support de communication (réseaux sociaux, web, médias), sont sous sa seule et entière responsabilité et, garantit MyVoice de toute réclamation ou recours qui pourrait être faite à son encontre sur des commentaires postés par le panéliste.</p>
                      <h3 id="partie6">6. Obligations de MyVoice</h3>
                      <p><strong>6.1. Accessibilité aux sites de MyVoice.</strong>&nbsp;MyVoice s’engage à fournir un accès gratuit à ses sites et ses applications. Le coût de la connexion au réseau Internet et des matériels utilisés pour consulter les sites demeure à la charge des panélistes.</p>
                      <p>Les serveurs de MyVoice sont disponibles 24 heures sur 24 et 7 jours sur 7, sauf pendant les opérations de maintenance et les interruptions de service, telles que définies ci-dessous.</p>
                      <p>MyVoice fera ses meilleurs efforts pour que le fonctionnement des sites et de ses applications ne soit pas interrompu et soit exempt de toute erreur. Elle ne garantit toutefois pas la continuité de cet accès, ni l’absence de dysfonctionnement de ces sites. Elle est tenue à cet égard d’une obligation de moyens.</p>
                      <p>Les sites, les applications et le réseau Internet peuvent notamment connaître des périodes de saturation en raison de l’encombrement de la bande passante, des coupures dues à des incidents techniques ou à des interventions de maintenance, des décisions de sociétés gérant lesdits réseaux ou, tout autre évènement indépendant de la volonté de MyVoice.</p>
                      <p>MyVoice peut aussi être contrainte de suspendre momentanément l’accès à ses sites et ses applications aux fins de réaliser des opérations de maintenance, de mises à jour ou d’améliorations techniques des dits sites ou applications, ou pour en faire évoluer le contenu et/ou leur présentation. MyVoice s’efforcera de réaliser ces opérations pendant les heures non-ouvrées et d’en limiter la durée. En cas d’intervention de maintenance d’urgence, MyVoice pourra suspendre tout ou partie de l’accès à ses sites pour réaliser les interventions techniques nécessaires.</p>
                      <p>MyVoice&nbsp;s’engage à informer préalablement les panélistes des maintenances prévues lorsque cela est possible.</p>
                      <p>En cas d’interruption de service ayant pour cause des faits échappant au contrôle de MyVoice (tel qu’un évènement de force majeure), et en cas de perte de données,&nbsp;MyVoice s’engage à restaurer la dernière version sauvegardée des sites et applications de&nbsp;MyVoice et des données.</p>
                      <p>Il est recommandé à chaque panéliste d’informer rapidement MyVoice de toute anomalie, erreur ou tout bug afin que MyVoice puisse mettre en œuvre tous les moyens nécessaires pour rétablir le fonctionnement normal dans les plus brefs délais.</p>
                      <p><strong>6.2. Sécurité des données.</strong>&nbsp;MyVoice s’efforce d’assurer la sécurité et l’intégrité des données recueillies dans le cadre de la conclusion et de l’exécution du contrat. Elle est tenue à cet égard d’une obligation de moyens.</p>
                      <p><strong>6.3. Mise à disposition d’outils.</strong>&nbsp;En fonction des panels, MyVoice met gracieusement à disposition des panélistes des outils de scan d’achats.</p>
                      <p>En tant que de besoin, elle prend à sa charge les opérations de maintien, de renouvellement, d’envoi et/ou de retour de ces outils.</p>
                      <h3 id="partie7">7. Contenus des panélistes</h3>
                      <p>Les panélistes pourront publier des contenus sur les sites et applications de MyVoice ou de tiers (page Facebook, blog Overblog, chaîne Youtube), notamment des commentaires, et pourront communiquer à MyVoice d’autres contenus dans le cadre de leur participation aux panels et aux études (ensemble les « Contenus »).</p>
                      <p>Les panélistes reconnaissent qu’ils sont tenus de se conformer aux législations et règlementations locales, étatiques, nationales et internationales en vigueur lors de l’utilisation de ces Contenus.</p>
                      <p>À titre non limitatif, les panélistes s’interdisent d’utiliser des Contenus :</p>
                      <ul>
                        <li>à caractère pédophile ou portant atteinte d’une quelconque manière aux mineurs ou à la protection des enfants et des adolescents ;</li>
                        <li>illégaux, nuisibles, menaçants, abusifs, constitutifs de harcèlement, diffamatoires, vulgaires, obscènes, haineux, racistes, homophobes, antisémites, xénophobes, révisionnistes ou autrement répréhensibles ;</li>
                        <li>pornographiques ;</li>
                        <li>contraires à l’ordre public et aux bonnes mœurs ;</li>
                        <li>qui pourraient être constitutifs, sans que ce qui suit soit limitatif, d’apologie ou d’incitation à la réalisation de crimes et délits, d’incitation au suicide, d’incitation à l’usage de drogues ou de substances interdites, d’activité illégale de jeu d’argent, d’incitation à commettre des actes de terrorisme ; de provocation à la discrimination, à la haine ou à la violence en raison de la race, de l’ethnie, de la religion ou de la nation, de fausses nouvelles ; de fausses rumeurs ; d’atteinte à l’autorité de la justice, aux procès, à la divulgation d’informations relatives à une situation fiscale individuelle ; de diffusion hors des conditions autorisées de sondages et simulations de vote relatifs à une élection ou un référendum ; de diffamations et injures ; ou encore d’actes mettant en péril des mineurs notamment par la fabrication, le transport, et la diffusion de messages à caractère violent ou pornographique ou de nature à porter gravement atteinte à la personne humaine et à sa dignité, à l’égalité entre les femmes et les hommes ;</li>
                        <li>portant atteinte à la vie privée et/ou à la protection des données à caractère personnel ;</li>
                        <li>faisant état ou incitant à la cruauté envers les animaux ;</li>
                        <li>qui violeraient tout brevet, marque de commerce, dessin et modèle déposé, secret de fabrication, droit d’auteur, droit voisin, droit de propriété intellectuelle ou tout autre droit de propriété appartenant à autrui, droit de la personnalité, ou dont la publication serait susceptible de constituer une faute délictuelle ou quasi-délictuelle ;</li>
                        <li>contenant des virus informatiques, bombes logiques, menace permanente avancée (<em>advanced persistent threat</em>) ou tout autre code, dossier ou programme conçu pour interrompre, détruire ou limiter la fonctionnalité de tout logiciel, ordinateur, système informatique ou outil de télécommunication sans que cette énumération soit limitative ;</li>
                        <li>ayant pour effet de diminuer, de désorganiser, d’empêcher l’utilisation normale des sites et applications de Analysis France, d’interrompre et/ou de ralentir la circulation normale des communications électroniques, par exemple via un envoi massif ;</li>
                        <li>incitant ou permettant tout acte de piratage informatique ou de contournement de dispositif technique de protection ou d’information/mention sur les droits de propriété intellectuelle ;</li>
                      </ul>
                      <p>Les panélistes reconnaissent que le non-respect de l’une ou plusieurs des interdictions précitées est susceptible d’entraîner la résiliation et/ou résolution du contrat dans les conditions prévues à l’article 12.</p>
                      <h3 id="partie8">8. Propriété intellectuelle</h3>
                      <p><strong>8.1. Contenu des sites et des applications.</strong>&nbsp;Le contenu des sites et des applications de MyVoice, notamment sa structure, son design, ses interfaces, les bases de données, les textes, images et éléments graphiques qui la composent ainsi que les balises HTML de référencement (méta-tags), à l’exclusion du contenu appartenant aux panélistes ou aux tiers, sont la propriété exclusive de MyVoice.</p>
                      <p>Les URL des sites, le nom « MyVoice », les noms des applications de MyVoice ainsi que tous les logos des services décrits sur les sites sont protégés par le droit d’auteur et le droit de la propriété industrielle. Les en-têtes de page, images personnalisés, icônes de fonction et scripts sont protégés par les droits de reproduction de MyVoice, ou constituent des marques de service, des marques commerciales et/ou l’identité visuelle de MyVoice.</p>
                      <p>Le panéliste est autorisé à représenter, sur son écran ou smartphone, les sites et applications de&nbsp;MyVoice uniquement pour consultation à titre temporaire et pour les besoins de la conclusion de l’exécution et de la fin du contrat. Toute reproduction, diffusion et/ou représentation, totale ou partielle, des sites et des applications, sous quelque forme que ce soit, sans l’autorisation expresse et préalable de MyVoice est interdite et serait constitutive d’une contrefaçon sanctionnée par les articles L.335-2 et suivants du Code de la propriété intellectuelle.</p>
                      <p>À ce titre, le panéliste s’interdit :</p>
                      <ul>
                        <li>de procéder à des extractions par transfert temporaire ou permanent, ou d’utiliser par la mise à disposition au public, la totalité ou une partie substantielle, en termes quantitatifs ou qualitatifs des sites et des applications de MyVoice et autres bases de données, visibles ou non, à des fins commerciales, statistiques ou autres ;</li>
                        <li>d’extraire ou d’utiliser de façon répétée et systématique tout ou partie des informations visibles sur les sites et les applications de MyVoice, lorsqu’une telle opération excède manifestement une utilisation normale à titre privé des sites ou des applications ;</li>
                        <li>de copier, imiter, modifier, utiliser exploiter, commercialiser ou distribuer tout élément constitutif des sites et applications de MyVoice, notamment les informations visibles sur les sites et les applications ou toute autre base de données, sans le consentement préalable écrit de MyVoice.</li>
                      </ul>
                      <p><strong>8.2. Marque MyVoice.</strong>&nbsp;Le panéliste s’interdit formellement d’utiliser la marque « MyVoice », sans l’accord écrit préalable de MyVoice. Toute représentation et/ou reproduction et/ou exploitation partielle ou totale de cette marque, de nature que ce soit, est totalement prohibée.&nbsp;MyVoice ne pourra être tenu responsable de l’utilisation frauduleuse de la marque «&nbsp;MyVoice » et/ou de ses logos.</p>
                      <p><strong>8.3 Propriété intellectuelle sur les Contenus.</strong>&nbsp;Le panéliste déclare être titulaire de tous les droits sur les Contenus nécessaires pour autoriser toutes les opérations visées au paragraphe suivant et que ces droits afférents aux Contenus n’ont pas fait l’objet d’une cession exclusive auprès de tiers. Le panéliste garantit MyVoice à cet égard contre toute action et/ou revendication de tiers.</p>
                      <p>En utilisant des Contenus dans le cadre du contrat ou en publiant des Contenus sur la page Facebook, la chaîne Youtube ou le blog de MyVoice , le panéliste concède à MyVoice une autorisation gratuite (notamment en raison de son caractère non-exclusif), non-exclusive, cessible et transférable, incluant le droit d’accorder des sous-licences, pour le monde entier et pour la durée légale de protection des droits, y compris toutes prorogations ou allongements, de reproduire, représenter, mettre à disposition, exploiter, extraire, éditer, adapter, modifier, distribuer ces Contenus directement ou indirectement, en tout ou partie, sur tous supports notamment les sites et les applications de MyVoice et supports de communication de MyVoice, en tout format actuel ou futur (notamment papier, vidéo, audiovisuel, numérique) et par tout moyen ou tout procédé de reproduction, de communication au public, de télécommunication ou de distribution, actuel ou futur, y compris numérique, quelles que soient les techniques d’exploitation utilisées, pour les besoins de MyVoice et notamment aux fins de communication, de promotion, de publicité et/ou d’information sur les activités de MyVoice, à titre gratuit ou payant, y compris à des fins commerciales.</p>
                      <p>Le panéliste est informé que MyVoice peut choisir de mettre en valeur tout ou partie des Contenus des panélistes, selon des modalités déterminées par MyVoice. En outre, l’exploitation des Contenus peut faire l’objet de partenariats et/ou constituer un support publicitaire sous réserve du droit au respect de la vie privée du panéliste, ce que le panéliste déclare accepter expressément.</p>
                      <p>Les autorisations délivrées en application du présent article portent sur les Contenus, déterminables et définis de la façon suivante :</p>
                      <ul>
                        <li>Contenus tels que définis aux présentes CGS,</li>
                        <li>communiqués, utilisés, publiés ou mis en ligne par le panéliste dans le cadre du présent contrat ou publiés sur la page Facebook, la chaîne Youtube et/ou le blog de MyVoice,</li>
                        <li>pendant la durée d’exécution du contrat.</li>
                      </ul>
                      <p>Ainsi, les autorisations délivrées portent sur l’ensemble desdits Contenus, et ce, dès la conclusion du contrat et compte tenu du caractère déterminable de celles-ci, le présent accord n’entre pas dans le cadre de la prohibition de la cession globale des œuvres futures de l’article L. 131-1 du Code de la propriété intellectuelle.</p>
                      <p>Toutefois, pour le cas où le panéliste serait amené à utiliser, publier ou mettre en ligne des contenus non encore déterminables au jour de la conclusion des présentes, ou encore pour le cas où les caractéristiques définies ci-dessus pourraient être considérées comme non suffisamment déterminantes, les parties ont tenu compte expressément de l’article L. 131-1 du Code de la propriété intellectuelle. Ainsi, en tant que de besoin, le panéliste s’engage à confirmer sans réserve à tout moment les autorisations délivrées à MyVoice, dès la mise en ligne, la publication ou l’utilisation du Contenu.</p>
                      <h3 id="partie9">9. Données personnelles</h3>
                      <p>La confidentialité des données des panélistes, les engagements de MyVoice et la manière dont MyVoice utilise et divulgue les données des panélistes sont détaillés dans la&nbsp;politique de confidentialité.</p>
                      <h3 id="partie10">10. Liste d’opposition au démarchage téléphonique pour les panélistes (Réunion)</h3>
                      <p>Le numéro de téléphone du panéliste est recueilli à l’occasion de l’inscription du panéliste.&nbsp;MyVoice s’engage à n’utiliser ces coordonnées téléphoniques que pour la bonne exécution du contrat.</p>
                      <p>Sans préjudice de ce qui précède, conformément aux articles L. 223-1 et suivants du Code de la consommation, le panéliste est informé qu’il dispose, si le souhaite, du droit de s’inscrire sur la liste d’opposition au démarchage téléphonique Bloctel, gérée par la société Opposetel, accessible sur le site :&nbsp;<a href="http://www.bloctel.gouv.fr/">http://www.bloctel.gouv.fr/</a>. L’inscription sur cette liste est gratuite. Elle s’impose à tous les professionnels à l’exception de ceux avec lesquels le panéliste a déjà conclu un contrat.</p>
                      <h3 id="partie11">11. Liens hypertextes</h3>
                      <p>Les sites de MyVoice peuvent intégrer des liens hypertextes en direction de sites web tiers (notamment Facebook, Youtube).</p>
                      <p>Sauf expressément indiqué sur ces sites web tiers, MyVoice n’exerce aucun contrôle des sites web tiers vers lesquels redirigent les liens hypertextes accessibles sur les sites. La consultation ou l’utilisation des sites web tiers sont régies par leurs propres conditions d’utilisation et/ou par le droit commun.</p>
                      <h3 id="partie12">12. Durée et résiliation</h3>
                      <p><strong>12.1. Durée.</strong>&nbsp;Le contrat prend effet à compter de l’acceptation par MyVoice de la proposition de participation émise par le panéliste lors de son inscription sur les sites de MyVoice.</p>
                      <p>Il s’agit d’un contrat à durée indéterminée qui prendra fin lors de sa résiliation selon les conditions prévues ci-après.</p>
                      <p><strong>12.2. Résiliation par le panéliste.</strong>&nbsp;Le panéliste peut, quel que soit le motif, mettre fin au contrat, sur simple demande, selon les modalités suivantes :</p>
                      <p><strong>Par lettre à l’adresse suivante</strong>&nbsp;:</p>
                      <p>MyVoice&nbsp;</p>
                      <p>Service de gestion des panélistes</p>
                      <p>8 rue de l’Amitié, ZAC du Triangle 97490, Sainte Clotilde, Réunion</p>
                      <p><strong>Par email à l’adresse suivante</strong>&nbsp;:</p>
                      <p><a href="mailto:myvoice974@ids.ac">myvoice974@ids.ac</a>&nbsp;</p>
                      <p><strong>Par le biais du formulaire disponible sur les sites</strong>&nbsp;:</p>
                      <p>Dans la rubrique « Nous contacter »</p>
                      <p>Le panéliste sera informé de la bonne prise en compte de sa demande de résiliation dans un délai maximum d’un (1) mois. Le contrat sera alors résilié dans un délai de trois (3) mois à compter de la date de confirmation de la prise en compte de la demande de résiliation par le panéliste afin de permettre à ce dernier d’utiliser son solde de points et, le cas échéant, de restituer l’outil de scan d’achats prêté par MyVoice.</p>
                      <p>MyVoice s’engage à confirmer la résiliation effective du panéliste, par email, téléphone ou courrier postal.</p>
                      <p><strong>12.3. Résiliation par MyVoice.</strong>&nbsp;MyVoice peut, quel que soit le motif, mettre fin au contrat. Le cas échéant, elle informera le panéliste de sa décision par écrit.</p>
                      <p>Le contrat sera alors résilié dans un délai de trois (3) mois à compter de la date de notification de la décision de résiliation par MyVoice sauf en cas de manquement grave et/ou répété du panéliste à ses obligations.</p>
                      <p>En cas de manquement grave et/ou répété du panéliste à ses obligations (par exemple, en cas de renseignement d’informations fausses, de multiples inscriptions ou d’irrégularités dans la communication des achats), le contrat sera alors résilié dès sa notification par MyVoice.</p>
                      <p><strong>12.4. Effets de la résiliation.</strong>&nbsp;La résiliation entraîne la fermeture de l’espace personnel du panéliste et la perte de tous les points non utilisés figurant sur son compte-points au jour de la résiliation. Le panéliste devra donc veiller à utiliser ses points avant cette date.</p>
                      <p>Les données relatives au panéliste seront détruites ou anonymisées dans les conditions définies dans la&nbsp;politique de confidentialité de MyVoice.</p>
                      <p>Le panéliste s’engage par ailleurs à renvoyer, dans les plus brefs délais à compter de la notification de la prise en compte de sa demande de résiliation ou de la notification de la décision de résiliation par MyVoice, l’outil de scan d’achats prêté par MyVoice. Dans l’hypothèse où l’outil de scan d’achats prêté au panéliste ne serait pas restitué,&nbsp;MyVoice se réserve le droit de demander une compensation financière.</p>
                      <h3 id="partie13">13. Modification du contrat</h3>
                      <p>La date de la dernière publication de mise à jour des présentes CGS est le 09/07/2020.</p>
                      <p>MyVoice pourra être amenée à modifier les présentes CGS, à supprimer certaines de ses clauses ou en ajouter de nouvelles.</p>
                      <p>Cette nouvelle version des CGS entrera en vigueur un (1) mois après sa mise en ligne pour les panélistes et candidats panélistes ayant déjà accepté le contrat. Le panéliste ou le candidat panéliste ayant déjà accepté une version antérieure des CGS sera réputé avoir accepté la nouvelle version des CGS un (1) mois après en avoir été informé. En cas d’écrit manifestant le refus par ce dernier de la nouvelle version des CGS, le contrat conclu avec le panéliste ou le candidat panéliste sera résilié dans les conditions définies à l’article 12.</p>
                      <p>Hormis ce que prévoit expressément et spécifiquement les CPS, les déclarations, accords, renonciations ou autres actes ou omissions de MyVoice ne sauraient en aucun cas être interprétés comme visant à modifier le contrat, et n’auront de valeur contraignante que s’ils sont sous forme écrite et signés par le panéliste et le représentant légal de MyVoice.</p>
                      <h3 id="partie14">14. Notifications</h3>
                      <p>Toute notification envoyée à MyVoice doit être adressée selon les modalités suivantes et doit inclure le nom du panéliste, son adresse email et les informations spécifiques concernant l’objet de la notification.</p>
                      <p><strong>Par lettre à l’adresse suivante</strong>&nbsp;:</p>
                      <p>MyVoice</p>
                      <p>Service de gestion des panélistes</p>
                      <p>8 rue de l’Amitié, ZAC du Triangle 97490, Sainte Clotilde, Réunion</p>
                      <p><strong>Par téléphone en appelant le numéro suivant</strong>&nbsp;: +230 202 0055</p>
                      <p><strong>Par email à l’adresse suivante</strong>&nbsp;:</p>
                      <p><a href="mailto:myvoice974@ids.ac">myvoice974@ids.ac</a></p>
                      <p><strong>Par le biais du formulaire disponible sur les sites</strong>&nbsp;:</p>
                      <p>Dans la rubrique « Nous contacter »</p>
                      <p>Le panéliste accepte que MyVoice lui adresse une notification ou toute autre information :</p>
                      <ul>
                        <li>en l’envoyant à l’adresse email indiquée lors de son adhésion,</li>
                        <li>en l’envoyant à l’adresse postale indiquée lors de son adhésion,</li>
                        <li>en l’appelant au téléphone,</li>
                        <li>ou en lui envoyant un SMS.</li>
                      </ul>
                      <p>Les notifications sont réputées être parvenues vingt-quatre (24) heures après l’envoi d’un email ou d’un SMS et une (1) semaine après l’envoi d’un courrier postal, sauf preuve contraire ou invalidité ou inexactitude de l’adresse communiquée.</p>
                      <h3 id="partie15">15. Stipulations diverses</h3>
                      <p><strong>15.1. Force majeure.</strong> MyVoice ne peut être tenue responsable d’aucun manquement à ses obligations au titre des présentes CGS ayant pour cause des faits échappant à son contrôle, le fait d’un tiers et/ou résultant d’un cas de force majeure. Les cas de force majeure comprendront notamment, outre les cas reconnus par la jurisprudence, toutes grèves, lock-out, y compris les cas de perturbation d’ordre technique, électromagnétique, défaillance des systèmes et des réseaux, et tous problèmes électriques et techniques externes aux parties et empêchant les communications.</p>
                      <p><strong>15.2. Non-validité partielle.</strong>&nbsp;Dans l’hypothèse où une ou plusieurs stipulations figurant dans les présentes CGS étaient considérées comme illégales, inopposables ou inapplicables par une décision de justice, les autres stipulations des CGS resteront en vigueur, à la condition que l’économie générale du contrat n’en soit pas bouleversée.</p>
                      <p><strong>15.3. Nature de la relation entre les parties.</strong>&nbsp;Le panéliste reconnaît que son adhésion et sa participation aux panels de consommateurs et/ou études de MyVoice ne créent pas une relation d’agence, de partenariat ou d’emploi et que celles-ci reposent exclusivement sur une base volontaire.</p>
                      <p><strong>15.4. Tolérance.</strong>&nbsp;Le fait de ne pas exiger ou forcer l’exécution par une partie de certaines des stipulations du contrat ne saurait être interprétée comme une renonciation, de la part de l’autre partie, à son droit d’exiger ou de faire imposer l’exécution de ces stipulations.</p>
                      <p><strong>15.5. Transfert du contrat. </strong>MyVoice&nbsp;pourra librement céder à tout moment tout ou partie de ses droits et obligations au titre du contrat (notamment par cession de fonds de commerce, apport d’actif, fusion).</p>
                      <p>Dans le cas où une telle cession serait susceptible d’engendrer une diminution des droits du panéliste aux termes de son contrat, MyVoice en informera préalablement le panéliste qui pourra, s’il refuse les termes de la cession, résilier immédiatement le contrat.</p>
                      <p><strong>15.6. Preuve.</strong>&nbsp;Les informations collectées par MyVoice dans le cadre du contrat sont susceptibles d’être conservées à titre de preuve pendant une durée de cinq (5) ans.</p>
                      <p>L’archivage de ces données est effectué par MyVoice sur un support fiable et durable.</p>
                      <h3 id="partie16">16. Droit applicable, compétence juridictionnelle et médiation</h3>
                      <p>Sans préjudice des dispositions impératives applicables au profit des consommateurs, les présentes CGS sont soumises à la loi mauricienne.</p>
                      <p>En cas de difficulté relative à la validité, l’interprétation et/ou l’exécution du contrat, le panéliste peut contacter MyVoice selon les modalités suivantes :</p>
                      <p><strong>Par lettre à l’adresse suivante</strong>&nbsp;:</p>
                      <p>MyVoice</p>
                      <p>Service de gestion des panélistes</p>
                      <p>8 rue de l’Amitié, ZAC du Triangle 97490, Sainte Clotilde, Réunion</p>
                      <p><strong>Par téléphone en appelant le numéro suivant</strong>&nbsp;: +230 202 0055</p>
                      <p><strong>Par email à l’adresse suivante</strong>&nbsp;:</p>
                      <p><a href="mailto:myvoice974@ids.ac">myvoice974@ids.ac</a>&nbsp;</p>
                      <p><strong>Par le biais du formulaire disponible sur les sites</strong>&nbsp;:</p>
                      <p>Dans la rubrique « Nous contacter »</p>
                      <p>En cas d’échec de la résolution du litige entre les parties, les parties peuvent saisir la juridiction compétente.</p>
                      <p>Le panéliste peut également saisir tout autre médiateur de la consommation ayant reçu l’agrément de la commission d’évaluation de la médiation de la consommation pour le secteur d’activités concerné.</p>
                      <p>Pour saisir un médiateur de la consommation, le panéliste devra au préalable s’adresser à MyVoice en adressant une lettre recommandée avec accusé de réception à l’adresse suivante:</p>
                      <p>MyVoice</p>
                      <p>Service Juridique de Analysis France SARL (Coordonnées du Médiateur)</p>
                      <p>8 rue de l’Amitié, ZAC du Triangle 97490, Sainte Clotilde, Réunion</p>
                      <p>A défaut de réponse dans un délai de deux (2) mois ou en cas de réponse insatisfaisante du service téléphonique de MyVoice, le panéliste pourra, s’il n’a pas déjà saisi une juridiction compétente et dans un délai maximum d’un (1) an suivant la date d’envoi de la première notification à MyVoice, recourir gratuitement au service de médiation pour les litiges de consommation découlant de l’exécution du contrat. Ne peuvent faire l’objet d’une revue par le médiateur les litiges pour lesquels la demande est manifestement infondée ou abusive ou a été précédemment examinée ou est en cours d’examen par un autre médiateur ou par un tribunal.</p>
                      <p>Pour soumettre le litige au médiateur, le panéliste peut le contacter ou tout autre médiateur de la consommation ayant reçu l’agrément de la Commission d’évaluation de la médiation de la consommation pour le secteur d’activités concerné.</p>
                      <p>Quel que soit le moyen utilisé pour saisir le médiateur, la demande du panéliste doit contenir les éléments suivants pour être traitée avec rapidité : coordonnées postales du panéliste, adresse email et numéro de téléphone ainsi que les nom et adresse complets de MyVoice, un exposé succinct des faits, et la preuve des démarches préalables auprès de MyVoice.</p>
                      <p>À défaut d’accord amiable, tout litige relatif à l’exécution et/ou l’interprétation des présentes CGS, et après échec de toute conciliation, sera soumis aux tribunaux compétents.</p></div>
                  </div>
                </article>
              </div>
            </div>
          </div>
        </section>
      </>
    )
  }
}

export default MuTermsCondition
