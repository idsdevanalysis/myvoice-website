import React, { Component } from 'react'
import Head from 'next/head'

class MuMentionLegal extends Component {
  render () {
    return (
      <>
        <Head>
          <title>Mentions légales - Réunion - myVoice</title>
        </Head>
        <section className="switchable">
                <div className="container">
                    <div className="row justify-content-between">
                        <div className="col-md-12 col-lg-12">
                        <h2>Mentions légales</h2>
                            <p className="lead">Merci de lire avec attention les différentes modalités d’utilisation du présent site avant d’y parcourir ses pages.&nbsp;<br/></p>
                            <p className="lead">En vous connectant sur ce site, vous acceptez, sans réserves, les présentes modalités. Aussi, conformément à l’article n°6 de la Loi n°2004-575 du 21 Juin 2004 pour la confiance dans l’économie numérique, les responsables du présent site internet www.myvoice.re<br/></p>
                            <p className="lead">La société éditrice du site &ldquo;myvoice.re&ldquo; <br/>Siège social : Analysis France SARL&nbsp;<br/>Siret : 880 865 274<br/>Adresse : 8, rue de l&apos;amitié - ZAC du Triangle 97490 Sainte Clothide<br/>Tél :&nbsp;262 262 92 10 10<br/><br/>Editeur du Site web de MyVoice : Interactive Data Services Ltd&nbsp;<br/>Adresse : Analysis House, Rue du Judiciaire, Ebene, Ile Maurice<br/>Tél : 202 0055<br/>Date de création : 05 février 2021<br/></p>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
  }
}

export default MuMentionLegal
