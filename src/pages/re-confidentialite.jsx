import React, { Component } from 'react'
import Head from 'next/head'

class RePrivacyPolicy extends Component {
  render () {
    return (
      <>
        <Head>
          <title>Politique de confidentialité - Réunion - myVoice</title>
        </Head>
        <section className="cover imagebg text-center height-50" data-overlay="3">
          <div className="background-image-holder"> <img alt="background" src="img/landing-3.jpg" /> </div>
          <div className="container pos-vertical-center">
            <div className="row">
              <div className="col-md-12">
                <h1>Politique de confidentialité de MyVoice</h1>
              </div>
            </div>
          </div>
        </section>
        <section>
          <div className="container">
            <div className="row justify-content-center">
              <div className="col-md-12 col-lg-12">
                <article>
                  <div className="et_pb_module et_pb_text et_pb_text_0  et_pb_text_align_left et_pb_bg_layout_light">

                    <div className="et_pb_text_inner">
                      <p>Version&nbsp;1.0</p>
                      <p>Date de dernière mise à jour&nbsp;: 07 juillet 2020</p>
                      <p>Plan :</p>
                      <ul>
                        <li><a href="#intro">MyVoice et la protection de vos données</a></li>
                        <li><a href="#partie1">1. COLLECTE ET UTILISATION DE VOS DONNÉES</a></li>
                        <li><a href="#partie2">2. LIEU DE STOCKAGE ET TRANSFERTS DE VOS DONNEES PERSONNELLES</a></li>
                        <li><a href="#partie3">3. SECURITE DE VOS DONNEES PERSONNELLES</a></li>
                        <li><a href="#partie4">4. DONNEES SENSIBLES COLLECTEES ET TRAITEES PAR MyVoice</a></li>
                        <li><a href="#partie5">5. EXACTITUDE DES DONNÉES</a></li>
                        <li><a href="#partie6">6. DONNÉES SUR LES MINEURS</a></li>
                        <li><a href="#partie7">7. DONNÉES SUR LES MEMBRES DE VOTRE FOYER</a></li>
                        <li><a href="#partie8">8. LES DROITS DU PANELISTE ET DES MEMBRES DE SON FOYER</a></li>
                        <li><a href="#partie9">9. LA PÉRIODE DE CONSERVATION DE VOS DONNEES</a></li>
                        <li><a href="#partie10">10. NOTIFICATION DES MODIFICATIONS DE LA PRESENTE POLITIQUE DE CONFIDENTIALITE</a></li>
                        <li><a href="#partie11">11. RECLAMATIONS OU DEMANDES</a></li>
                        <li><a href="#partie12">12. COMMENT NOUS CONTACTER</a></li>
                      </ul>
                      <p id="intro"><strong>MyVoice et la protection de vos données</strong></p>
                      <p>MyVoice organise et gère des panels de consommateurs (le(s) «&nbsp;panel(s)&nbsp;») qui sont des groupes représentatifs de personnes participant à une ou plusieurs études. Ces panels et études consistent à fournir des informations ou répondre à des questions sur sa consommation, ses habitudes de vie, ses opinions, ses centres d’intérêts, etc. Les participants à de tels panels et/ou études spécifiques sont appelés membres du panel ou panélistes (les «&nbsp;panélistes&nbsp;»).</p>
                      <p>Dans le cadre de ces activités, MyVoice collecte des données personnelles concernant les panélistes et les membres de leur foyer. Par «&nbsp;données personnelles&nbsp;», on entend toute information concernant une personne physique qui peut être utilisée pour l’identifier directement ou indirectement. Il peut s’agir du nom, prénom, numéro de téléphone, photographie, date de naissance, lieu de résidence,&nbsp;<em>etc</em>.</p>
                      <p>MyVoice accorde une importance particulière à la protection des données personnelles et de la vie privée des panélistes et de leur foyer. MyVoice veille donc à respecter les nouvelles normes du Data Protection Act du 22 décembre 2017 relatif à la protection des personnes physiques à l’égard du traitement des données à caractère personnel et à la libre circulation de ces données (le « RGPD »).</p>
                      <p>La présente politique de confidentialité (la «&nbsp;politique de confidentialité&nbsp;») explique aux panélistes comment MyVoice utilise et protège leurs données personnelles et celles des membres de leurs foyers.</p>
                      <p>Le responsable des traitements de données est Analysis France SARL («&nbsp;MyVoice »), immatriculée au Registre du Commerce du Gouvernement mauricien, sous le numéro&nbsp;C08014948 , dont le siège social est situé Analysis House, Rue du Judiciaire, Ebene, Ile Maurice.&nbsp;</p>
                      <p>Cette politique de confidentialité s’applique aux données personnelles transmises à MyVoice par les panélistes notamment par l’intermédiaire des sites Internet et/ou applications mobiles dédiés aux panelistes, édités par MyVoice ou ses sous-traitants, mais également aux données personnelles des panélistes transmises par des tiers à MyVoice.</p>
                      <p>MyVoice recommande aux panélistes et aux membres de leur foyer de lire attentivement cette politique de confidentialité.</p>
                      <h3 id="partie1"><strong>1. COLLECTE ET UTILISATION DE VOS DONNÉES</strong></h3>
                      <p>Les données des panélistes et des membres de leur foyer sont recueillies par MyVoice lors de leur inscription aux panels et/ou à une ou plusieurs études spécifiques, au cours de la participation aux panels et/ou études spécifiques et lors de la résiliation de leur adhésion à l’un des panels et/ou à l’une des études spécifiques, par plusieurs biais, et notamment à l’aide d’outils de scans des achats, de formulaires, de questionnaires, de cookies et auprès de tiers.</p>
                      <p><strong>Votre inscription à un panel et/ou à des études spécifiques</strong></p>
                      <p>Certaines données personnelles sont recueillies lors de votre inscription à un panel ou à une étude spécifique. Ces données sont nécessaires à MyVoice :</p>
                      <ul>
                        <li>pour vous contacter&nbsp;: nom et prénom(s), adresse e-mail, adresse postale et numéros de téléphone fixe et/ou mobile,</li>
                        <li>afin d’évaluer votre admissibilité aux différents panels&nbsp;ou à une étude spécifique :</li>
                        <ul>
                          <li>informations sociodémographiques: âge (y compris la date de naissance), sexe, profession et informations sur la composition du foyer (nom(s) et prénom(s) des personnes composant le foyer, date(s) de naissance, sexe, situation(s) professionnelle(s),&nbsp;<em>etc</em>.</li>
                          <li>informations sur votre mode de vie et vos habitudes d’achat et de consommation,</li>
                          <li>des informations relatives à votre équipement informatique et vos possibilités d’accès à Internet.</li>
                        </ul>
                      </ul>
                      <p>Les données sont fournies à MyVoice par le panéliste et après accord de tous les autres membres du foyer adultes et légalement responsables, et après avoir eu l’accord de(s) personnes ayant l’autorité parentale ou du tuteur légal pour tous les autres membres du foyer mineurs ou adultes non juridiquement responsables, sur une base volontaire.</p>
                      <p>Les données sont nécessaires pour la validation de l’inscription et pour l’évaluation de l’admissibilité aux différents panels de consommateurs. L’ensemble des réponses du formulaire d’inscription sont obligatoires pour participer à l’un des panels et/ou à l’une des études spécifiques, sauf mention contraire sur les supports de collecte de données. Cette exigence de fourniture de données a ici un caractère contractuel.</p>
                      <p><strong>Votre participation aux panels et/ou à des études spécifiques</strong></p>
                      <p>Dans le cadre des panels de consommateurs et/ou études spécifiques, le panéliste est invité à répondre à des questionnaires et éventuellement à utiliser des outils pour scanner ses achats afin de transmettre à MyVoice des informations sur ses habitudes de consommation, son état de santé, son comportement, ses centres d’intérêts et/ou ses opinions. Il peut s’agir par exemple d’informations portant sur la fréquence à laquelle vous faites vos courses, sur les rayons de magasins ou les produits que vous préférez,&nbsp;<em>etc</em>.</p>
                      <p>Les réponses aux questionnaires sont, sauf mention contraire, obligatoires pour obtenir le nombre de points correspondant affiché sur le questionnaire.</p>
                      <p>Toutefois, certaines réponses à certains questionnaires peuvent être obligatoires même en l’absence de points à gagner, essentiellement pour les questionnaires sur les enquêtes de qualité de participation aux panels et/ou études spécifiques et les questionnaires de recueil de consentement.</p>
                      <p>Ces exigences de fourniture de données ont un caractère contractuel.</p>
                      <p>MyVoice recueille également, sauf opposition du panéliste, des données sur les panélistes auprès de tiers disposant de données sur ces derniers (obtenus avec le consentement des panélistes ou sur une autre base juridique).</p>
                      <p>Dans le cadre de ce recueil d’informations auprès de tiers, MyVoice peut aussi être amenée à communiquer à ces tiers des informations sur les panélistes, notamment, afin que ces tiers puissent identifier (recenser) dans leurs propres bases de données les personnes qui sont aussi des panélistes de MyVoice et qu’ils fournissent à MyVoice des informations sur les panélistes notamment sur leurs comportements d’achats, leurs habitudes ou leurs centres d’intérêts. Ces tiers peuvent avoir collecté directement ces données auprès des panélistes (par exemple par le biais de la fourniture de services aux panélistes) ou avoir eux-mêmes recueilli tout ou partie des données communiquées à MyVoice auprès d’autres tiers, et ce, avec le consentement du panéliste ou sur une autre base juridique.</p>
                      <p>Ces données sont nécessaires à MyVoice pour&nbsp;:</p>
                      <ul>
                        <li>Réaliser des études, analyses et un suivi de vos habitudes de vie et de consommation, votre comportement et opinions en tant que consommateurs afin de permettre aux clients et partenaires de MyVoice d’affiner leur connaissance de leurs propres clients et prospects et d’améliorer la personnalisation de leurs produits, services ou actions marketing et publicitaires. Le tableau ci-après vous donne des précisions sur la manière dont nous effectuons nos études, analyses et suivi et sur la manière dont nous communiquons vos données à nos clients ou à des tiers.</li>
                        <li>Vous contacter par e-mail, par téléphone, par notifications ou messages sur téléphone portable ou par voie postale pour&nbsp;:</li>
                        <ul>
                          <li>vous assister dans le cadre de votre participation aux panels et/ou études spécifiques (types groupes de discussion, triades, testeurs de produits) ;</li>
                          <li>vous rappeler de participer à une étude ou de finaliser un questionnaire (comme nos études portent notamment sur l’évolution de la consommation et des modes de vie, il est utile que les panélistes y participent avec une certaine régularité)&nbsp;;</li>
                          <li>obtenir des informations complémentaires à la suite d’une ou plusieurs réponses données lors d’un questionnaire&nbsp;(par exemple, pour vous demander pourquoi vous n’avez pas déclaré vos achats depuis plus de trois semaines ou pourquoi depuis peu vous déclarez l’achat de produit pour animaux, etc.) ;</li>
                          <li>vous informer des mises à jour de nos services&nbsp;;</li>
                          <li>vous envoyer notre newsletter, sauf opposition de leur part&nbsp;;</li>
                          <li>vous tenir informé(e) du nombre de points obtenus en récompense de votre participation à un panel et/ou à une étude spécifique.</li>
                        </ul>
                        <li>Gérer vos points et vous envoyer les cadeaux attribués en contrepartie de votre inscription et participation.</li>
                        <li>Évaluer votre éligibilité aux différents panels et/ou aux études spécifiques au fur et à mesure de votre participation au panel et aux études spécifiques.</li>
                        <li>Protéger MyVoice de comportements frauduleux ou contraires à nos Conditions Générales et/ou Particulières de Services.</li>
                      </ul>
                      <p><strong>Collecte des données par le biais de cookies et applications logicielles</strong></p>
                      <p>Des cookies sont utilisés sur les sites de MyVoice. Les cookies sont des petits fichiers stockés sur votre ordinateur par un site Internet qui attribue un identifiant numérique et fournit certaines informations sur votre navigation en ligne. Ils sont utilisés par les développeurs Web pour aider les utilisateurs à naviguer efficacement sur les sites Internet et pour exécuter certaines fonctionnalités. Le site Internet envoie des informations au navigateur qui crée ensuite un fichier. Chaque fois que l’utilisateur retourne sur le même site Internet, le navigateur récupère et envoie ce fichier au serveur du site Internet.</p>
                      <p>Pour nos études, analyses et suivi de vos habitudes et de vos comportements, nous utilisons des cookies optionnels et/ou des applications logicielles optionnelles nous permettant de suivre votre navigation sur Internet, mais seulement si vous avez donné votre consentement explicite à de tels cookies/applications.</p>
                      <p>Pour plus d’informations sur l’utilisation des cookies sur les sites de MyVoice et la possibilité de s’opposer à leur utilisation, consultez la politique de cookies de Analysis France SARL&nbsp;<a href="https://kantartns.io/politique-de-confidentialite-france/">ici</a>.</p>
                      <p><strong>Collecte de vos données pour se conformer aux obligations légales et réglementaires</strong></p>
                      <p>MyVoice peut être amenée à collecter des données permettant de respecter ses obligations en matière de gestion de ses plateformes en ligne, de vos droits, ou de litiges. Dans ce cadre, nous sommes amenés à collecter des documents justifiant de votre identité (ainsi que votre qualité à exercer un droit pour autrui le cas échéant) et des données relatives aux contenus publiés sur nos plateformes en ligne, sur une demande d’exercice de droits, à un litige.</p>
                      <p>La non-communication des justificatifs précités peut empêcher&nbsp;MyVoice de répondre à vos demandes et/ou à ses obligations légales et/ou réglementaires.</p>
                      <p>Cette exigence de fourniture de données a ici un caractère réglementaire.</p>
                      <p><strong>Collecte de vos données en cas de résiliation&nbsp;</strong></p>
                      <p>Sur réception d’une demande de mettre fin à la participation à un panel ou à une étude spécifique, MyVoice peut vous demander de répondre à un très court questionnaire pour comprendre les raisons pour lesquelles vous souhaitez mettre fin à votre activité de panéliste ou à votre participation à une étude spécifique. La réponse est facultative, mais permet à MyVoice d’améliorer la gestion de ses panels et de ses études spécifiques pour les futurs membres ou les futures études spécifiques.</p>
                      <p><strong>Les fondements juridiques des traitements de vos données</strong></p>
                      <p>En application de la réglementation, tout traitement de données à caractère personnel doit, pour être licite, reposer sur l’un des fondements juridiques énoncés à l’article 6 du RGPD ou à l’article 9.2 du RGPD s’il s’agit de données dites sensibles. Ces fondements ou bases juridiques sont énumérés ci-dessous et sont susceptibles d’être différents pour chaque cas d’utilisation&nbsp;de vos données :</p>
                      <ul>
                        <li>vous avez consenti à ce que nous utilisions vos données personnelles (de manière explicite en cas de collecte de données sensibles)</li>
                        <li>ces données nous sont nécessaires afin d’exécuter un contrat conclu avec vous</li>
                        <li>ces données nous sont nécessaires pour respecter nos obligations légales</li>
                        <li>ces données nous sont nécessaires afin de protéger vos intérêts vitaux ou ceux de quelqu’un d’autre</li>
                        <li>ces données nous sont nécessaires pour effectuer une tâche d’intérêt public, ou</li>
                        <li>ces données nous sont nécessaires pour servir nos intérêts légitimes (ou ceux de nos clients&nbsp;; dans ce cas, nous expliquerons la nature de ces intérêts.</li>
                      </ul>
                      <p><strong>Précisions sur la manière dont MyVoice utilise vos données&nbsp;</strong></p>
                      <p>Vous trouverez ci-dessous des informations plus détaillées sur la façon dont nous utilisons vos données personnelles.</p>
                      <table border="1">
                        <tbody>
                          <tr>
                            <td><strong>Finalités</strong></td>
                            <td><strong>Précisions</strong></td>
                            <td><strong>Catégories de données collectées/traitées</strong></td>
                          </tr>
                          <tr>
                            <td><strong>Études de marché</strong></td>
                            <td>Analyse de votre comportement d’achat et de consommation, de vos opinions en tant que consommateur et de vos centres d’intérêts afin de réaliser des études statistiques [ex : quantité moyenne achetée par acheteur sur une période donnée d’un produit donné, l’évolution de la quantité moyenne achetée par acheteur sur plusieurs périodes qui peut être un indicateur du succès d’une opération promotionnelle]</td>
                            <td>Identifiant panéliste, coordonnées, adresse e-mail, voix, image, vos réponses aux questionnaires et études, vos données relatives à votre consommation, votre opinion, et plus généralement toutes les données dont nous disposons sur vous et votre foyer.</td>
                          </tr>
                          <tr>
                            <td><strong>Études scientifiques pour des universitaires, des organismes de santé publique ou des instituts de recherche</strong></td>
                            <td>Recherche dans le secteur de la santé et de la recherche publique (par exemple sur la sociologie des comportements d’achats et l’évolution des comportements, les stratégies d’offres des industriels, l’innovation alimentaire, l’impact de la consommation sur la santé des populations, les inégalités nutritionnelles, l’accès à l’alimentation saine)</td>
                            <td>Identifiant panéliste, coordonnées, adresse e-mail, données de santé (p. ex. maladie, état de santé, diagnostic, type de traitement, réponses à des questionnaires autour de du thème de la satisfaction permettant d’identifier des besoins des consommateurs non satisfaits)</td>
                          </tr>
                          <tr>
                            <td><strong>Études scientifiques pour des entreprises commerciales et des organismes de recherche caritatifs</strong></td>
                            <td>Recherche dans le secteur de la santé et de la recherche privée (par exemple sur les besoins futurs du consommateur, les facteurs de consommation et/ou de dé-consommation, les indices d’un changement de comportement ou d’un besoin)</td>
                            <td>Identifiant panéliste, coordonnées, adresse e-mail, données de santé (p. ex. maladie, état de santé, diagnostic, type de traitement, réponses à des questionnaires autour de du thème de la satisfaction permettant d’identifier des besoins des consommateurs non satisfaits)</td>
                          </tr>
                          <tr>
                            <td><strong>Échanges ou fourniture de données individualisées « pseudonymisées » avec des tiers (sans suivi régulier des réponses)</strong></td>
                            <td>
                              <p>Lorsque vous participez à nos études, nous utilisons généralement un identifiant de panéliste fixe. Lorsque nous partageons des données individualisées avec un client ou un partenaire, nous pouvons créer un identifiant temporaire pour ce tiers, afin qu’il ne puisse pas connaître votre identifiant de panéliste. Nous ne lui communiquons pas non plus votre identité ou vos coordonnées.</p>
                              <p>Cela permet aux clients ou partenaires d’associer les réponses/données d’un profil à des comportements sans avoir accès à votre identité directement.</p>
                              <p>On parle de données « pseudonymes » et non « anonymes » parce qu’il est possible pour Analysis France de vous ré-identifier à partir de l’identifiant temporaire. Il est aussi possible que notre client dispose d’informations complémentaires sur vous qui lui permette de vous identifier à partir des données que nous lui transmettons. Il ne s’agit donc pas de données totalement anonymes.</p>
                            </td>
                            <td>Identifiant temporaire unique associé à un projet spécifique et autres données collectées sur le panéliste et son foyer en fonction de l’étude concernée ou de la demande du client</td>
                          </tr>
                          <tr>
                            <td><strong>Échanges ou fourniture de données individualisées « pseudonymisées » avec des tiers (avec suivi régulier des réponses&nbsp;des personnes régulièrement interrogées) (projets de suivi)</strong></td>
                            <td>Pour certains de nos clients ou partenaires, nous avons besoin de fournir un identifiant non temporaire afin de pouvoir suivre l’évolution de votre opinion ou de votre comportement sur une période de temps spécifique. Pour ce type de projet particulier que nous appelons « projets de suivi », nous utiliserons des identifiants permanents qui seront communiqués à notre client ou partenaire au début de chacun de ces projets. Les projets de ce type incluront une notification aux panélistes soit en toute première page du questionnaire ou de l’étude soit par email d’information, vous permettant d’identifier ces projets et de décider si vous souhaitez ou non y participer.</td>
                            <td>Identifiant permanent unique associé à un projet spécifique et autres données collectées sur le panéliste et son foyer en fonction de l’étude concernée ou de la demande du client</td>
                          </tr>
                          <tr>
                            <td><strong>Combinaison et enrichissement de données</strong></td>
                            <td>
                              <p>Nous enrichissons les données que nous détenons vous concernant en combinant vos données personnelles avec celles de tiers (voir le détail des catégories de tiers ci-dessus).</p>
                              <p>Nous compilons aussi vos données avec les données d’autres consommateurs pour créer des groupes de consommateurs ou de panélistes correspondant à un même mode de vie ou un même comportement.</p>
                              <p>Ensuite, le prestataire de services de couplage (notre partenaire) détient les données personnelles que nous partageons avec lui pendant une courte période, couple les données avec d’autres informations et nous renvoie les renseignements combinés. Les partenaires sont tenus par contrat de supprimer les données que nous partageons avec eux et ne sont pas autorisés à les utiliser autrement que dans ce but spécifique.</p>
                            </td>
                            <td>Identifiant permanent unique, coordonnées, adresse e-mail, identifiants de réseaux sociaux (Facebook, Twitter, etc.), cookie, identifiant d’appareil mobile, numéro d’identification officiel</td>
                          </tr>
                          <tr>
                            <td><strong>Ciblage publicitaire et recherches d’achat de médias</strong></td>
                            <td>
                              <p>Nous utilisons vos données personnelles pour aider nos clients et nos fournisseurs à proposer des publicités plus pertinentes.</p>
                              <p>Plus précisément, grâce à votre participation à nos études et à vos données, nous pouvons aider nos clients à améliorer leur ciblage publicitaire en créant des profils plus précis de consommateurs ou d’internautes ayant les mêmes caractéristiques, les mêmes centres d’intérêts, les mêmes habitudes ou les mêmes modes de consommation, par le biais de combinaison de données et d’extrapolations. Ces données et profils sont couplés avec les données et/ou les cookies de partenaires du secteur de la publicité et du marketing. Nous incluons des garanties contractuelles afin de veiller à ce que vous ne soyez pas spécifiquement ciblé(e) à des fins commerciales ou publicitaires à la suite de l’utilisation de vos données pour permettre la création d’un profil de ciblage publicitaire ou d’audience, et à ce que nos partenaires ne puissent pas utiliser vos données à d’autres fins que la création de profils de ciblage publicitaire ou d’audience.</p>
                            </td>
                            <td>Identifiant unique permanent, coordonnées, adresse e-mail, identifiants de réseaux sociaux (Facebook, Twitter, etc.), cookie, adresse IP, identifiant d’appareil mobile, numéro d’identification officiel</td>
                          </tr>
                          <tr>
                            <td><strong>Exposition publicitaire et mesures d’audience</strong></td>
                            <td>En plus de la combinaison de données basée sur les cookies (que vous pouvez contrôler et accepter depuis votre compte de paneliste), nous utiliserons les données personnelles que vous nous fournissez, telles que votre adresse e-mail, dans le cadre d’un processus de mise en correspondance de nos bases de données avec celles de tiers (nos clients et partenaires) pour déterminer si vous êtes un utilisateur de leurs services (tels que des réseaux sociaux, des sites Internet ou des applications mobiles) à des fins d’études en matière de mesure d’audience de la publicité. Plus précisément, nous identifierons les publicités auxquelles vous avez été exposé(e) sur les sites et plateformes de ces tiers et nous mesurerons ensuite l’impact de ces publicités sur votre consommation, vos habitudes, comportements, opinions. Les tiers avec lesquels nous travaillons ne sont pas autorisés à utiliser vos données à d’autres fins que celles mentionnées ci-dessus.</td>
                            <td>Identifiant panéliste, coordonnées, adresse e-mail, identifiants de réseaux sociaux (Facebook, Twitter, etc.), cookie, adresse IP, identifiant d’appareil mobile, numéro d’identification officiel</td>
                          </tr>
                          <tr>
                            <td><strong>Gestion d’une procédure judiciaire, d’un litige et/ou d’une requête émanant des autorités publiques</strong></td>
                            <td>Communication d’informations aux autorités compétentes en application d’exigences légales ou réglementaires ou en cas de contentieux.</td>
                            <td>Identifiant panéliste, nom, coordonnées, adresse e-mail, cadeaux ou points obtenus par le panéliste en raison de sa participation.</td>
                          </tr>
                          <tr>
                            <td><strong>Lutte contre la fraude et la violation des&nbsp;Conditions Générales et/ou Particulières de Services&nbsp;de Analysis France</strong></td>
                            <td>Lutte contre les comportements frauduleux ou les comportements non conformes aux Conditions Générales et/ou Particulières de Services de Analysis France, notamment prévention des entrées multiples par les mêmes personnes.</td>
                            <td>Adresse IP, caractéristiques du navigateur, caractéristiques de l’appareil, adresses postales, adresses e-mail, numéros de téléphone, identifiant panéliste, cookies</td>
                          </tr>
                        </tbody>
                      </table>
                      <br />
                      <h3 id="partie2"><strong>2. LIEU DE STOCKAGE ET TRANSFERTS DE VOS DONNEES PERSONNELLES</strong></h3>
                      <p>Les serveurs de stockage de données de MyVoice sont situés en Europe et à Maurice et sont gérés par des fournisseurs de services tiers dans le cloud. Les données peuvent aussi être stockées ou accessibles à des fins de traitement de données par le personnel technique de nos prestataires en Inde.</p>
                      <p>Nous mettons en place des garanties suffisantes pour assurer que le transfert est effectué de façon sécurisée et légale, conformément à la législation en matière de protection des données au sein de l’UE :</p>
                      <ul>
                        <li>le cas échéant, si la Commission européenne a rendu une décision d’adéquation, reconnaissant à ce pays un niveau de protection des données personnelles équivalent à celui prévu par la législation de l’Espace économique européen (EEE), vos données seront transférées sur ce fondement ;</li>
                        <li>si le niveau de protection n’a pas été reconnu comme équivalent par la Commission européenne (comme c’est la cas de l’Inde), nous nous appuyons soit sur une dérogation prévue par le RGPD et applicable à la situation, soit sur la mise en place d’une des garanties appropriées pour assurer la protection de vos données personnelles (clauses contractuelles types approuvées par la Commission européenne, certification par l’organisme au Bouclier de Protection des Données (Privacy Shield) ou tout accord qui lui succèdera en cas de transfert vers les Etats-Unis).</li>
                      </ul>
                      <p>Si vous souhaitez obtenir des informations sur ces garanties, veuillez adresser une demande de la manière indiquée au point 11 du présent document.</p>
                      <p>&nbsp;</p>
                      <h3 id="partie3"><strong>3. SECURITE DE VOS DONNEES PERSONNELLES</strong></h3>
                      <p>Nous prenons les mesures technologiques et organisationnelles appropriées pour protéger vos données personnelles, aussi bien pendant la transmission de celles-ci qu’une fois que nous les recevons. Nos procédures de sécurité sont conformes aux normes généralement acceptées et utilisées pour protéger les données personnelles.</p>
                      <p>Tous nos employés sont contractuellement tenus de suivre nos politiques et procédures en matière de confidentialité, de sécurité et de respect de la vie privée.</p>
                      <p>Vos informations de compte et vos données personnelles sont protégées par mot de passe . Afin de garantir la sécurité de vos données personnelles, nous vous recommandons de ne pas divulguer votre mot de passe ou vos données d’authentification à qui que ce soit. Analysis France ne vous demandera jamais votre mot de passe lors d’un appel téléphonique ou d’un e-mail même s’ils émanent de salariés de Analysis France SARL. En outre, pensez à vous déconnecter de votre compte dans la mesure du possible et à fermer la fenêtre de votre navigateur lorsque vous avez terminé de visiter notre site ou nos applications. Cela permet d’éviter que d’autres personnes n’accèdent à vos données et échanges personnels si vous partagez un ordinateur avec quelqu’un d’autre ou si vous utilisez un ordinateur dans un lieu public. Nous veillerons à ce que, par mesure de sécurité, vous changiez régulièrement votre mot de passe.</p>
                      <p><strong></strong></p>
                      <h3 id="partie4"><strong>4. DONNEES SENSIBLES COLLECTEES ET TRAITEES PAR MYVOICE</strong></h3>
                      <p>Certaines des données collectées et traitées par MyVoice peuvent être qualifiées de «&nbsp;sensibles&nbsp;». Pour information&nbsp;: les données «&nbsp;sensibles&nbsp;» sont des données relatives à l’origine raciale ou ethnique, aux opinions politiques, aux croyances religieuses ou philosophiques, à l’appartenance syndicale, à la santé ou à la vie sexuelle ou l’orientation sexuelle d’une personne physique.</p>
                      <p>Ces données peuvent être recueillies par le biais de questionnaires mais également par d’autres biais, notamment en utilisant un outil de scan des achats. Par exemple, en fonction les tâches exigées par certains panels, vous pouvez être amenés à indiquer la nature des achats effectués et certains achats pourraient révéler votre état de santé (l’achat de paracétamol peut indiquer que vous souffrez d’un mal de tête). Sauf autorisation expresse de votre part, nous ne divulguons jamais les données «&nbsp;sensibles&nbsp;» de façon individuelle, mais nous produisons des statistiques sur la base de ces données (par exemple&nbsp;: les personnes de plus de tel âge achètent du paracétamol tous les quinze jours en moyenne).</p>
                      <p>Le caractère sensible d’une donnée vous sera signalé lorsque celle-ci est recueillie par questionnaire. L’absence de réponse à ces questionnaires n’affectera pas votre qualité de panéliste.</p>
                      <p>Votre refus de fournir certaines données sensibles par le biais de questionnaires n’affectera pas votre qualité de panéliste. Nous n’insisterons jamais pour que les panélistes fournissent des données sensibles.</p>
                      <p>Certaines données peuvent aussi indirectement se révéler «&nbsp;sensibles&nbsp;»&nbsp;: par exemple la composition de votre foyer peut permettre d’en déduire votre probable orientation sexuelle&nbsp;; votre alimentation peut aussi permettre de déduire votre probable orientation religieuse.</p>
                      <h3 id="partie5"><strong>5. EXACTITUDE DES DONNÉES</strong></h3>
                      <p>Nous prenons des mesures raisonnables pour conserver les données personnelles en notre possession ou sous notre contrôle exactes, complètes et à jour, sur la base des informations les plus récentes mises à notre disposition par vous et/ou par nos clients et partenaires.</p>
                      <p>Nous comptons sur vous pour nous aider à conserver vos données personnelles exactes, complètes et à jour en répondant honnêtement à nos questions. Vous devez vous assurer de nous informer de toute modification de vos données personnelles.</p>
                      <p>Pour nous informer de tout changement, veuillez visiter la section «&nbsp;Contact&nbsp;» du site Internet destiné aux panélistes ou la section «&nbsp;Comment nous contacter&nbsp;» ci-dessous.</p>
                      <h3 id="partie6"><strong>6. DONNÉES SUR LES MINEURS</strong></h3>
                      <p>MyVoice reconnaît la nécessité de mettre en place des mesures de protection de la vie privée supplémentaires pour les données personnelles collectées auprès ou sur des personnes mineures. Nous n’invitons jamais sciemment des mineurs de moins de 15 ans à être le principal acheteur du foyer au sein de nos panels. Toutefois, nous pouvons demander aux acheteurs principaux majeurs qui ont des enfants dans leur foyer de participer à des tâches ou de fournir des informations qui impliquent la collecte de données concernant directement ou indirectement leurs enfants mineurs. Les acheteurs principaux majeurs doivent alors consentir expressément à la collecte et au traitement des données des mineurs en leur qualité de parent ou de tuteur légal de ces mineurs. MyVoice peut alors être amenée à solliciter des justificatifs prouvant que l’acheteur principal dispose bien de l’autorité parentale.</p>
                      <p>S’il est nécessaire et approprié pour un projet particulier d’impliquer directement des enfants de moins de quinze ans, nous prenons des mesures pour nous assurer que nous avons reçu l’autorisation d’un parent ou du tuteur légal. MyVoice fournira aux parents et tuteurs des informations sur le sujet de l’enquête, toutes les informations personnelles ou sensibles susceptibles d’être collectées auprès des enfants, la façon dont ces données seront utilisées et si MyVoice est susceptible de partager ces informations et avec qui, et ce pour chaque étude.</p>
                      <p>Nous obtiendrons le consentement par e-mail ou sur une page Web. Nous expliquerons quelles informations nous recueillons, comment nous prévoyons de les utiliser et comment les parents peuvent donner et retirer leur consentement. Sans consentement, le mineur ne pourra pas participer à la tâche.</p>
                      <p>Si un mineur prend part aux activités du panel, il est de la responsabilité des personnes titulaires de l’autorité parentale de le superviser.</p>
                      <h3 id="partie7"><strong>7. DONNÉES SUR LES MEMBRES DE VOTRE FOYER</strong></h3>
                      <p>En participant à nos panels ou études spécifiques, vous pouvez être amenés à nous communiquer des informations concernant des membres de votre foyer (votre conjoint, votre colocataire, vos enfants, vos parents, etc.).</p>
                      <p>Le traitement de ces données doit être fait dans le respect de la réglementation.</p>
                      <p><strong>Vous devez vérifier et vous assurer que vous avez le droit de communiquer ces données à&nbsp;MyVoice (par exemple en ayant l’accord écrit de ces personnels) et leur communiquer les informations sur le traitement de leurs données par MyVoice figurant dans la présente&nbsp;politique de confidentialité</strong>.</p>
                      <h3 id="partie8"><strong>8. LES DROITS DU PANELISTE ET DES MEMBRES DE SON FOYER</strong></h3>
                      <p>Dans les conditions définies aux articles 15 et suivants du RGPD, vous disposez, sauf exceptions&nbsp;:</p>
                      <ul>
                        <li>du droit d’obtenir de&nbsp;MyVoice la confirmation que des données personnelles vous concernant sont ou ne sont pas traitées et, lorsqu’elles le sont, l’accès auxdites données à caractère personnel ainsi qu’à plusieurs informations sur nos traitements (droit d’accès – article 15 du RGPD)&nbsp;;</li>
                        <li>du droit d’obtenir de MyVoice la rectification des données personnelles vous concernant qui sont inexactes (droit de rectification – article 16 du RGPD)&nbsp;;</li>
                        <li>du droit d’obtenir de MyVoice l’effacement de données personnelles vous concernant, dans certains cas (droit d’effacement ou «&nbsp;droit à l’oubli&nbsp;» – article 17 du RGPD)&nbsp;;</li>
                        <li>du droit d’obtenir de MyVoice la limitation des traitements, dans certains cas (droit à la limitation du traitement – article 18 du RGPD)&nbsp;;</li>
                        <li>du droit d’obtenir à tout moment de MyVoice, pour de raisons tenant à votre situation particulière, que nous ne procédions plus aux traitements des données personnelles vous concernant, dans certains cas (droit d’opposition – article 21.1 du RGPD)&nbsp;;</li>
                        <li>du droit de retirer votre consentement pour les traitements fondés sur votre consentement (droit de retrait du consentement – article 7.3 du RGPD)</li>
                        <li>du droit de vous opposer, à tout moment, au traitement des données personnelles vous concernant à des fins de prospection (droit d’opposition à la prospection – article 21.2 du RGPD)&nbsp;;</li>
                        <li>du droit de définir, modifier et révoquer à tout moment des directives relatives à la conservation, à l’effacement et à la communication de vos données personnelles après votre mort, en application de l’article 40-1 de la loi Informatique et Libertés. Ces directives peuvent être générales ou particulières. Nous pouvons être uniquement dépositaires des directives particulières concernant les données que nous traitons, les directives générales peuvent être recueillies et conservées par un tiers de confiance numérique certifié par la CNIL.</li>
                      </ul>
                      <p>Vous avez également le droit de désigner un tiers auquel les données vous concernant pourront être communiquées après votre mort. Vous vous engagez alors à informer ce tiers de votre démarche et du fait que des données permettant de l’identifier sans ambiguïté nous seront transmises, à lui communiquer la présente Politique de Confidentialité.</p>
                      <p>Sous réserve de justifier de votre identité et des éléments susvisés, le panéliste ou le membre du foyer du panéliste concerné par les données peut exercer les droits définis ci-dessus en contactant Analysis France, en utilisant la section «&nbsp;Contact&nbsp;» du site ou en nous écrivant soit à l’adresse postale suivante :</p>
                      <p>Par courrier à l’adresse suivante&nbsp;:</p>
                      <p>MyVoice</p>
                      <p>Service de gestion des panélistes (RGPD)</p>
                      <p>8 rue de l’Amitié, ZAC du Triangle 97490, Sainte Clotilde, Réunion</p>
                      <p>Soit par email à l’adresse suivante :</p>
                      <p><a href="myvoice974@ids.ac">myvoice974@ids.ac</a></p>
                      <p>Soit Par le biais du formulaire disponible sur les Sites :</p>
                      <p>Dans la rubrique «&nbsp;Nous contacter&nbsp;»</p>
                      <p>Il vous est possible de procéder à certaines rectifications ou suppressions directement à partir de votre compte en ligne de panéliste sur nos sites Internet ou applications.</p>
                      <h3 id="partie9"><strong>9. LA&nbsp;PÉRIODE DE CONSERVATION&nbsp;DE VOS DONNEES</strong></h3>
                      <p>Des enquêtes de MyVoice observent le comportement des consommateurs sur le long terme, nécessitant plusieurs années d’étude des données traitées par MyVoice. Les données sont conservées pendant toute la période de participation au panel et pour une période de cinq ans à compter du 1er&nbsp;janvier suivant la date à laquelle le panéliste met fin à sa participation au panel.</p>
                      <p>Les données des candidats au panel non retenus sont conservées pendant un an glissant.</p>
                      <h3 id="partie10"><strong>10. NOTIFICATION DES MODIFICATIONS DE LA PRESENTE POLITIQUE DE CONFIDENTIALITE</strong></h3>
                      <p>Nous nous réservons le droit de procéder à tout moment à des modifications, ajouts ou suppressions concernant la présente politique de confidentialité. Lisez cette page régulièrement pour vous assurer de prendre connaissance de tout changement. Toutefois, tout changement substantiel de la présente politique de confidentialité vous sera communiqué. Les changements non substantiels de cette politique de confidentialité seront annoncés sur le site uniquement. Si vous restez membre du panel et continuez à utiliser nos services après ces changements, nous en conclurons que vous les acceptez.</p>
                      <p>Nous fournirons toujours la politique la plus récente sur notre site Internet ou nos applications.</p>
                      <p>Nous révisons régulièrement notre politique de confidentialité. La présente politique de confidentialité a été mise à jour pour la dernière fois 07/07/2020.</p>
                      <h3 id="partie11"><strong>11. RECLAMATIONS OU DEMANDES</strong></h3>
                      <p>MyVoice s’efforce de respecter les normes les plus élevées lors de la collecte et de l’utilisation de renseignements personnels. Si vous pensez que notre façon de collecter ou d’utiliser ces renseignements est injuste, trompeuse ou inappropriée, nous vous encourageons à nous le faire savoir. Nous apprécions également les suggestions pour améliorer nos procédures.</p>
                      <h3 id="partie12"><strong>12. COMMENT NOUS CONTACTER</strong></h3>
                      <p>Si vous avez des questions concernant notre gestion de vos données personnelles ou si vous souhaitez demander des informations sur notre politique de confidentialité, notre Déléguée à la Protection des données est Madame Gillie Abbotts-Jones et vous pouvez nous contacter :</p>
                      <p>Par courrier à l’adresse suivante&nbsp;:</p>
                      <p>MyVoice</p>
                      <p>Service de gestion des panélistes (RGPD)</p>
                      <p>8 rue de l’Amitié, ZAC du Triangle 97490, Sainte Clotilde, Réunion</p>
                      <p>Soit par email à l’adresse suivante :</p>
                      <p><a href="mailto: myvoice974@ids.ac">myvoice974@ids.ac</a></p>
                      <p>Soit Par le biais du formulaire disponible sur les Sites :</p>
                      <p>Dans la rubrique «&nbsp;Nous contacter&nbsp;»</p></div>
                  </div>
                </article>
              </div>
            </div>
          </div>
        </section>
      </>
    )
  }
}

export default RePrivacyPolicy
