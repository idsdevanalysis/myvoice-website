import React, { Component } from 'react'
import Head from 'next/head'

class FaqPage extends Component {
  render () {
    return (
      <>
        <Head>
          <meta name="description" content="Comment fonctionne le panel MyVoice et comment s'inscrire ?" />
          <title>FAQ - myvoice</title>
        </Head>
        <section className="cover imagebg text-center height-50" data-overlay="3">
          <div className="background-image-holder"> <img alt="background" src="img/landing-3.jpg" /> </div>
          <div className="container pos-vertical-center">
            <div className="row">
              <div className="col-md-12">
                <h1> Des questions ?&nbsp;</h1>
              </div>
            </div>
          </div>
        </section>
        <section>
          <div className="container">
            <div className="row justify-content-center">
              <div className="col-md-12 col-lg-12">
                <article>
                  <div className="article__body">
                    <p>1. Qu&apos;est-ce que le Panel MyVoice ? <br />MyVoice est une plateforme en ligne réunissant des personnes de tout âge, pour participer dans des discussions sur plusieurs sujets d&apos;actualité ou sur des marques. Votre avis sera sollicité à travers des questionnaires en ligne, des groupes de discussion ou encore des entretiens individuels. Vous avez aussi la possibilité de participer à des tests de produits ! <br /><br />2. Qui est derrière le Panel MyVoice ? <br />Le Panel MyVoice a été créé par Analysis Co Ltd. Ce panel de consommateurs permet de proposer à ses clients des études et des analyses fiables sur les habitudes de vie et de consommation et le comportement des consommateurs dans la zone océan Indien. <br /><br />3. Qui peut devenir membre ? <br />Le panel MyVoice est ouvert à tous les internautes sans aucune restriction. Les membres doivent disposer d’une adresse e-mail afin de recevoir les invitations à participer aux enquêtes et un accès à Internet. <br /><br />4. Est-ce que l&apos;inscription au Panel MyVoice est gratuite ? <br />Oui, l&apos;inscription au Panel MyVoice est gratuite et vous pouvez la résilier à tout moment, sans aucune condition. <br /><br />5. A quels types d&apos;études pourrai-je répondre ? <br />Les membres du Panel sont interrogés régulièrement sur des thématiques variées : sondages sur l&apos;actualité, test de nouveauté, étude sur vos modes de vie, etc. <br /><br />

                    6. J&apos;ai oublié mon mot de passe, comment faire ? <br />
                    En cas d&apos;oubli de mot de passe, vous avez la possibilité de le réinitialiser : allez sur la page d&apos;accueil et cliquez sur le bouton « Mot de passe perdu ? ». Sur la page suivante, insérez votre l&apos;email adresse et cliquez sur « Continuer ». Cliquez sur « Renvoyer un email » et veuillez garder la fenêtre ouverte. Vous recevrez un email avec un code de vérification. Vous pourrez par la suite modifier votre mot de passe. <br /><br />

                    7. Comment savoir si mon inscription est validée ?<br />Une fois que vous aurez validé le formulaire d’inscription, vous recevrez un courriel confirmant que nous avons reçu le formulaire. Il contiendra un lien vers un questionnaire nous permettant de mieux vous connaître. Vous devrez compléter ce premier questionnaire pour finaliser votre inscription au panel. Une fois inscrit, vous recevrez un second courriel vous confirmant votre statut de panéliste. <br /><br />8. Suis-je obligé de répondre aux enquêtes que je reçois ? <br />Seul le premier questionnaire permettant de mieux vous connaître est obligatoire : si vous n’y répondez pas, vous ne pourrez pas devenir panéliste. À partir du moment où vous avez obtenu le statut de panéliste, rien ne vous oblige à répondre aux enquêtes qui vous sont adressées. <br /><br />9. Que faire des points cumulés ? <br />En répondant aux questionnaires qui vous sont adressés, vous accumulez un certain nombre de points. Quand vous atteignez 425 points, vous pouvez alors commencer à échanger vos points. Allez sous l’option « Espace Cadeaux » qui se trouve en haut à droite de votre interface. Cliquez dessus pour visualiser les bons d&apos;achats qui vous sont proposés, puis commander. Une fois la fiche de commande complétée, cliquez sur « Envoyer », de là notre service reprendra contact pour valider la commande. Votre bon d&apos;achat sera transmis par voie postal sous un délai de deux semaines au maximum après la validation de la commande. <br /><br />
                    10. Est-ce que je peux parrainer mes proches ? <br />
                    Oui, c&apos;est possible ! Vous pouvez parrainer au maximum 5 personnes et obtenir 150 points par inscrit.* Pour parrainer, allez sur la page d&apos;accueil de votre compte, cliquez sur l&apos;option « Parrainage ». Insérez le prénom et nom de votre proche ainsi que son adresse e-mail dans les cases correspondantes. Puis cliquez sur « Envoyer les invitations ». Dès que votre filleul aura complété son inscription, vous recevrez automatiquement vos 150 points par inscrit.  Précisez bien à vos filleuls, qu&apos;après la création de leur compte, ils recevront un e-mail de validation. Notez que cet e-mail peut atterrir dans le dossier spam. Celui-ci les invitera à confirmer leur adresse courriel et à se connecter pour la première fois à leur compte pour remplir le questionnaire d&apos;entrée, tout comme vous l&apos;aviez fait.<br />
                    *Condition : Le parrainage ne doit pas inclure les membres d’un même foyer.<br /><br />

                    11. Comment se désinscrire du panel ?<br />À tout moment, vous pouvez vous désinscrire du panel en vous connectant à votre espace personnel « Mon compte panéliste » et en cliquant sur « Se désinscrire ».&nbsp;&nbsp;</p>
                  </div>
                </article>
              </div>
            </div>
          </div>
        </section>
      </>
    )
  }
}

export default FaqPage
