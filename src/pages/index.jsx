import React, { Component } from 'react'
import Head from 'next/head'

class HomePage extends Component {
  render () {
    return (
      <>
        <Head>
          <meta name="description" content="MyVoice, un panel de consommateurs en ligne, pour faciliter les échanges et permettre aux marques d'avoir des solutions rapides." />
          <title>myvoice</title>
        </Head>
        <a id="cover-page" className="in-page-link"></a>
        <section className="d-block d-sm-none d-none d-sm-block d-md-none mobile">
          <div><img alt="background" src="img/run.png" /> </div>
          <div className="container text-center">
            <p></p>
                <h2> Donnez votre avis, participez à des groupes de discussion, testez des produits et gagnez de multiples cadeaux.</h2>
                <p className="lead"> Inscrivez-vous facilement et rapidement, c&apos;est gratuit !&nbsp; et obtenez 200 points de bienvenue</p>
                <div className="dropdown">
                  <a className="btn btn--primary type--uppercase" href="/sinscrire">inscrivez-vous</a>
                </div>
                {/* <a className="btn btn--primary type--uppercase" href="https://research.kantartns.io/community/myVoice/myvoice/Account/SignUp" target="_blank" rel="noreferrer"> <span className="btn__text">
                  inscrivez-vous gratuitement</span> </a> */}
          </div>

        </section>
        <section className="cover imagebg text-center parallax height-80 d-none d-md-block d-lg-none d-none d-lg-block d-xl-none d-none d-xl-block" data-overlay="3">
          <div className="background-image-holder"> <img alt="background" src="img/run.png" /> </div>
          <div className="container pos-vertical-center">
            <div className="row">
              <div className="col-md-8 col-lg-8">
                <h1> Donnez votre avis, participez à des groupes de discussion, testez des produits et gagnez de multiples cadeaux.</h1>
                <p className="lead"> Inscrivez-vous facilement et rapidement, c&apos;est gratuit !&nbsp;</p>
                <p className="lead">Et obtenez 200 points de bienvenue</p>
                <div className="dropdown">
                  <a className="btn btn--primary type--uppercase" href="/sinscrire">inscrivez-vous</a>
                </div>
                {/* <a className="btn btn--primary type--uppercase" href="https://research.kantartns.io/community/myVoice/myvoice/Account/SignUp" target="_blank" rel="noreferrer"> <span className="btn__text">
                  inscrivez-vous gratuitement</span> </a> */}
              </div>
            </div>
          </div>
        </section>
        <a id="cest-quoi-myvoice-" className="in-page-link"></a>
        <section className="switchable space--xxs switchable--switch suscribe">
          <div className="container">
            <h2>Rejoignez MyVoice et participez à des études indemnisées.</h2>
            <div className="row">
              <div className="col-md-9">
                <div className="row">
                  <div className="col-sm-4"><h3>Quoi?</h3></div>
                  <div className="col-sm-8">MyVoice.re est une plateforme en ligne réunissant les Réunionnais de tout âge  qui vous permet de participer et discuter autour de plusieurs sujets d&apos;actualité ou de donner votre opinion sur des marques.</div>
                </div>
                <div className="row">
                  <div className="col-sm-4"><h3>Comment?</h3></div>
                  <div className="col-sm-8">Via des questionnaires en ligne, des groupes de discussion, des entretiens individuels et même des tests produits ! Facile !</div>
                </div>
                <div className="row">
                  <div className="col-sm-4"><h3>Pourquoi?</h3></div>
                  <div className="col-sm-8">Donner votre avis, gagner des chèques cadeaux, des bons d’achat et participer à des tirages au sort.</div>
                </div>
              </div>
              <div className="col-md-3 text-right text-xs-center"><a className="btn btn--primary type--uppercase" href="/sinscrire">inscrivez-vous</a></div>
            </div>
          </div>
        </section>
        <a id="coin-membre" className="in-page-link"></a>
        <section className="text-center bg--dark space--xxs">
          <div className="container">
            <div className="row">
              <div className="col-md-10 col-lg-8">
                <h2>Inscrivez-vous sur MyVoice !</h2>
              </div>
            </div>
          </div>
        </section>
        <a id="process-comment-sinscrire" className="in-page-link"></a>
        <section className="text-center bg--dark space--xxs">
          <div className="container">
            <div className="row justify-content-center">
              <div className="col-md-3">
                <a href="#process-comment-sinscrire" className="block">
                  <div className="feature boxed boxed--border border--round"> <img src="/img/clock.png" /> <span className="h5 color--primary"><p>3 min seulement pour s&apos;inscrire</p></span> </div>
                </a>
              </div>
              <div className="col-md-3">
                <a href="#process-comment-sinscrire" className="block">
                  <div className="feature boxed boxed--border border--round"> <img src="/img/tel.png" /> <span className="h5 color--primary"><p>Contact direct par message ou mail pour ne rien rater </p></span> </div>
                </a>
              </div>
              <div className="col-md-3">
                <a href="#process-comment-sinscrire" className="block">
                  <div className="feature boxed boxed--border border--round"> <img src="/img/notif.png" /> <span className="h5 color--primary"><p>Sondages et tests produits courts et faciles</p></span> </div>
                </a>
              </div>
              <div className="col-md-3">
                <a href="#process-comment-sinscrire" className="block">
                  <div className="feature boxed boxed--border border--round"> <img src="/img/Mail.png" /> <span className="h5 color--primary"><p>Des points à échanger contre les cadeaux de votre choix</p></span> </div>
                </a>
              </div>
            </div>
          </div>
        </section>
        <section className="text-center bg--dark space--xxs">
          <div className="container">
            <div className="row">
              <div className="col-md-10 col-lg-8">
                <h2>Les garanties de MyVoice</h2>
              </div>
            </div>
          </div>
        </section>
        <section className="text-center bg--dark space--xxs garanties">
          <div className="container">
            <div className="row">
              <div className="col-md-4">
                <div className="feature feature--featured feature-1 boxed">
                  <h5>Respect<br /> de vos données personnelles</h5>
                </div>
              </div>
              <div className="col-md-4">
                <div className="feature feature--featured feature-1 boxed">
                  <h5>Zéro publicité, zéro spam,<br /> c’est promis !</h5>
                </div>
              </div>
              <div className="col-md-4">
                <div className="feature feature--featured feature-1 boxed">
                  <h5>Vos données<br /> restent anonymes</h5>
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col">
                <a className="btn btn--primary type--uppercase" href="/sinscrire" data-placement="left">inscrivez-vous</a>
              </div>
            </div>
          </div>
        </section>
        <a id="espace-cadea" className="in-page-link"></a>
        <section className="text-center space--xs">
          <div className="container">
            <div className="row">
              <div className="col-md-10 col-lg-8">
                <h1>Comment ça marche ?</h1>
                <h2>Répondez aux enquêtes, gagnez des points, échangez les contre des cadeaux</h2>
                <p className="lead"> Dans l’espace cadeau de MyVoice, le catalogue vous propose de nombreux cadeaux et bons d’achat. Vous trouverez des produits de loisir, de bricolage, déco et jardinage, restauration, des vêtements et accessoires, des sorties, et encore plein d’autres cadeaux !</p>
                <p>
                  <a className="btn btn--primary type--uppercase" href="/sinscrire" data-placement="left">inscrivez-vous</a>
                </p>
              </div>
            </div>
          </div>
        </section>
        <a id="des-ides-pour-gagner-des-points" className="in-page-link"></a>
        <section className="text-center space--xxs">
          <div className="container">
            <div className="row">
              <div className="col-md-10 col-lg-8">
                <h2>Et pour gagner encore plus de points ?</h2>
              </div>
            </div>
          </div>
        </section>
        <a id="process-gagner-des-points" className="in-page-link"></a>
        <section className="space--xxs">
          <div className="container">
            <div className="process-2 row">
              <div className="col-md-3">
                <div className="process__item">
                  <h5>Missions de parrainage : 10 personnes = 2000 points</h5>
                </div>
              </div>
              <div className="col-md-3">
                <div className="process__item">
                  <h5>Vous êtes un membre actif ? C&apos;est votre anniversaire ? Vous recevez 100 points !!</h5>
                </div>
              </div>
              <div className="col-md-3">
                <div className="process__item">
                  <h5>Participez à des groupes de discussion</h5>
                </div>
              </div>
              <div className="col-md-3">
                <div className="process__item">
                  <h5>Devenez testeur de produits</h5>
                </div>
              </div>
            </div>
          </div>
        </section>
      </>
    )
  }
}

export default HomePage
