import React from 'react'
import Document, { Html, Head, Main, NextScript } from 'next/document'
import Layout from '../components/Layout/Layout'

class MyDocument extends Document {
  static async getInitialProps (ctx) {
    const initialProps = await Document.getInitialProps(ctx)
    return { ...initialProps }
  }

  render () {
    return (
      <Html>
        <Head>
          <link href="/css/stack-interface.css" rel="stylesheet" type="text/css" media="all" />
          <link href="/css/socicon.css" rel="stylesheet" type="text/css" media="all" />
          <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
          <link href="/css/iconsmind.css" rel="stylesheet" type="text/css" media="all" />
          <link href="/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
          <link href="/css/flickity.css" rel="stylesheet" type="text/css" media="all" />
          <link href="/css/stack-interface.css" rel="stylesheet" type="text/css" media="all" />
          <link href="/css/theme.css" rel="stylesheet" type="text/css" media="all" />
          <link href="/css/custom.css?v=2" rel="stylesheet" type="text/css" media="all" />
          <link href="/css/jquery.cookiebar.css" rel="stylesheet" type="text/css" media="all" />
          <link href="https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700" rel="stylesheet" />
          <script async src="https://www.googletagmanager.com/gtag/js?id=G-29J6C0R9SL"></script>
          <script dangerouslySetInnerHTML={{ __html: 'window.dataLayer = window.dataLayer || [];function gtag() {dataLayer.push(arguments);}gtag("js", new Date()); gtag("config", "G-29J6C0R9SL");' }}>
        </script>
        </Head>
        <body>
          <div id="fb-root"></div>
          <Layout>
            <Main />
          </Layout>
          <div className="fb-customerchat"
            attribution="install_email"
            page_id="110069444520703"
            theme_color="#E85931"
            logged_in_greeting="Bonjour, comment puis-je vous aider?"
            logged_out_greeting="Bonjour, comment puis-je vous aider?">
          </div>
          <NextScript />
          <script src="/js/jquery-3.1.1.min.js"></script>
          <script src="/js/bootstrap.bundle.min.js"></script>
          <script src="/js/flickity.min.js"></script>
          <script src="/js/parallax.js"></script>
          <script src="/js/smooth-scroll.min.js"></script>
          <script src="/js/scripts.js"></script>
          <script src="/js/jquery.cookiebar.js"></script>
          <script src="/js/custom.js"></script>
        </body>
      </Html>
    )
  }
}

export default MyDocument
