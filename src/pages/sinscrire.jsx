import React, { Component } from 'react'
import Head from 'next/head'

class RegisterPage extends Component {
  render () {
    return (
      <>
        <Head>
          <meta name="description" content="Redirection vers s'inscrire ?" />
          <title>M&apos;incrire - myvoice</title>
        </Head>
        <section id="redirect" className="text-center height-50" data-overlay="3" data-redirect="https://research.kantartns.io/community/reunion/myvoice/Account/SignUp">
        <h2>Redirection dans 5 secondes vers le lien suivant</h2>
        <a className="btn btn--primary type--uppercase" href="https://research.kantartns.io/community/reunion/myvoice/Account/SignUp">inscrivez-vous</a>
        </section>
      </>
    )
  }
}

export default RegisterPage
