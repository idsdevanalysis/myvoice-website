import React, { Component } from 'react'
import Head from 'next/head'
import ContactForm from '../components/Layout/components/ContactForm/ContactForm'

class ContactPage extends Component {
  render () {
    return (
      <>
        <Head>
          <meta name="description" content="Comment fonctionne le panel MyVoice by Kantar et comment s'inscrire ?" />
          <title>Contact - myvoice</title>
        </Head>
        <a id="coin-client" className="in-page-link"></a>
        <section className="text-center space--xs bg--dark">
          <div className="container">
            <div className="row">
              <div className="col-md-10 col-lg-8">
                <h2>Coin client : Pour vos projets</h2>
                <p className="lead"><b>Un Pulse </b>survey en moins de 15 jours<b> </b>! Désormais possible grâce à <b>MyVoice</b> ! Demandez un devis. <br /><br /> <b>La possibilité de réaliser rapidement une étude de marché, grâce au digital et à notre communauté en ligne, </b><b>MyVoice</b><b>, tout en optimisant votre budget.</b></p>
              </div>
            </div>
          </div>
        </section>
        <a id="request-for-quote-form" className="in-page-link"></a>
        <section className="switchable switchable--switch">
          <div className="container">
            <div className="row justify-content-between">
              <div className="col-lg-6">
                <div className="switchable__text">
                  <h2>Vous recherchez une solutions rapide pour :&nbsp;</h2>
                  <p className="lead">• Vérifier l’acceptabilité d’un nouveau prix <br /> • Évaluer l’attractivité d’un nouveau concept ou packaging <br /> • Tester une campagne publicitaire et des visuels <br /> • Mesurer la notoriété et l’image de vos marques <br />• Recueillir des opinions sur un projet <br /> • Évaluer l’intérêt pour une nouvelle offre&nbsp;&nbsp;</p>
                </div>
              </div>
              <div className="col-lg-6">
                <ContactForm />
              </div>
            </div>
          </div>
        </section>
      </>
    )
  }
}

export default ContactPage
