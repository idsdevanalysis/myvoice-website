/* eslint-disable no-undef */
/* eslint-disable eqeqeq */
/* eslint-disable camelcase */
(function ($) {
  if (typeof window !== 'undefined') {
    // Cookie Bar
    $.cookieBar({
      message: 'Nous utilisons des cookies pour nous permettre de mieux comprendre comment le site est utilisé. En continuant à utiliser ce site, vous acceptez cette politique. En cliquant sur J\'accepte", vous acceptez l\'utilisation des cookies.',
      acceptText: 'J\'accepte'
    })

    // ZOHO Form
    try {
      const f = document.createElement('iframe')
      f.src = 'https://forms.zohopublic.com/virtualoffice18797/form/MyVoiceCoinclient/formperma/9eOVe0akTP2scRz4tTPIwkxydu5zQyCsOHmNXwM6qaU?zf_rszfm=1'
      f.style.border = 'none'
      f.style.height = '990px'
      f.style.width = '90%'
      f.style.transition = 'all 0.5s ease'// No I18N
      const d = document.getElementById('zf_div_9eOVe0akTP2scRz4tTPIwkxydu5zQyCsOHmNXwM6qaU')
      d.appendChild(f)
      window.addEventListener('message', function () {
        const zf_ifrm_data = event.data.split('|')
        const zf_perma = zf_ifrm_data[0]
        const zf_ifrm_ht_nw = (parseInt(zf_ifrm_data[1], 10) + 15) + 'px'
        const iframe = document.getElementById('zf_div_9eOVe0akTP2scRz4tTPIwkxydu5zQyCsOHmNXwM6qaU').getElementsByTagName('iframe')[0]
        if ((iframe.src).indexOf('formperma') > 0 && (iframe.src).indexOf(zf_perma) > 0) {
          const prevIframeHeight = iframe.style.height
          if (prevIframeHeight != zf_ifrm_ht_nw) {
            iframe.style.height = zf_ifrm_ht_nw
          }
        }
      }, false)
    } catch (e) { }

    // Facebook chat
    window.fbAsyncInit = function () {
      FB.init({
        xfbml: true,
        version: 'v10.0'
      })
    }

    // redirect
    $('#redirect').each(function () {
      const url = $(this).data('redirect')
      setTimeout(function () {
        window.location.href = url
      }, 5000)
    });

    // facebook msg
    (function (d, s, id) {
      let js; const fjs = d.getElementsByTagName(s)[0]
      if (d.getElementById(id)) return
      js = d.createElement(s); js.id = id
      js.src = 'https://connect.facebook.net/fr_FR/sdk/xfbml.customerchat.js'
      fjs.parentNode.insertBefore(js, fjs)
    }(document, 'script', 'facebook-jssdk'))
  }
})(jQuery)
