# MyVoice Website

## Next.js

### Start server for development

```bash
npm start
```

### Build static HTML files

```bash
npm run deploy
```